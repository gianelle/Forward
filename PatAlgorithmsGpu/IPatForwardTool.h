// $Id: IPatForwardTool.h,v 1.2 2007-11-19 15:11:29 aperiean Exp $
#ifndef TRACKINTERFACES_IPATFORWARDTOOL_H 
#define TRACKINTERFACES_IPATFORWARDTOOL_H 1

// Include files

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

#include "Event/Track.h"
#include "PatFwdTypes.h"

//static const InterfaceID IID_IPatForwardTool ( "IPatForwardTool", 1, 0 );

/** @class IPatForwardTool IPatForwardTool.h TrackInterfaces/IPatForwardTool.h
 *  Interface to the forward pattern tool
 *
 *  @author Olivier Callot
 *  @date   2005-10-04
 */
class IPatForwardTool : public extend_interfaces<IAlgTool> {
public: 

  // Return the interface ID
  //static const InterfaceID& interfaceID() { return IID_IPatForwardTool; }
  DeclareInterfaceID( IPatForwardTool, 2, 0 );

  virtual void forwardTrack( const LHCb::Track& track, LHCb::Tracks& output ) const = 0;
  virtual void setNNSwitch( bool nnSwitch) = 0;
  //virtual void convert(LHCb::Track*, OutTrack, LHCb::Track*) const ;
  virtual void deserializeTracks(LHCb::Tracks*, const  std::vector<uint8_t>   &, LHCb::Tracks* ) const = 0;


//protected:

//private:

};
#endif // TRACKINTERFACES_IPATFORWARDTOOL_H
