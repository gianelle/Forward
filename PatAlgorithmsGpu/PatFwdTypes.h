#ifndef PATFWD_TYPES
#define PATFWD_TYPES 1

#include <vector>

#define FWDHITS 40

struct OutTrack {
// output tracks
  int vtid; // velo track id
  // fit parameters
  double  ax;
  double  bx;
  double  cx;
  double  dx;
  double  ay;
  double  by;
  // "dof" values
  double  chi2xdof;
  double  ndof;
  // quality
  double  quality;
  // lhcbID hits
  int HitsNum;
  int Hits[FWDHITS];
};

struct FWDEvent {

// detector geometry <-- to define!
 
 
// Velo tracks

  int noVeloTks;

  std::vector<int>      vt_id;
  std::vector<float>    vt_x0;
  std::vector<float>    vt_tx;
  std::vector<float>    vt_y0;
  std::vector<float>    vt_ty;
  std::vector<float>    vt_z0;
  std::vector<float>    vt_qoverp;
   
// Hits  

  int noOTHits;
  
  std::vector<int>      oth_LhcbID;
  std::vector<int>      oth_channelID;
  std::vector<float>    oth_xAtYEq0;
  std::vector<float>    oth_zAtYEq0;
  std::vector<float>    oth_yBegin;
  std::vector<float>    oth_yEnd;
  std::vector<float>    oth_weight;
  std::vector<float>    oth_driftDistance;
  std::vector<int>      oth_next;
  std::vector<int>      oth_prev;

};

#endif

