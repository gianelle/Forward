#ifndef PATFWD_SERIALIZATION
#define PATFWD_SERIALIZATION 1

//-----------------------------------------------------------------------------
// PATFWDSerializer
//
// 2015-07-10 : Alessio Gianelle @ PD.INFN 
//-----------------------------------------------------------------------------

#include <cstdint>
#include <vector>
#include <memory>

#include <algorithm>
#include <iterator>
#include <exception>
#include <iostream>

#include "PatFwdTypes.h"
#include "PatKernel/PatForwardHit.h"
#include "Event/Track.h"
//#include "IPatForwardTool.h"


class PatFwdSerialization {
public:
  typedef std::vector<uint8_t> Data; // vector of chars

public:
  
  void cleanEvent();
  void addHit(PatFwdHit* hit);
  void addVeloTk(LHCb::Track *veloTk, unsigned int);
  void serializeEvent(Data & buffer);
//  void deserializeTracks(LHCb::Tracks*, IPatForwardTool*, const Data &, LHCb::Tracks* );

private:
  FWDEvent m_event;

};

#endif

