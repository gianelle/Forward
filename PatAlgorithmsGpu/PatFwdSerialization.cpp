//-----------------------------------------------------------------------------
// PatForwardSerialization
//
// 2014-07-10 : Alessio Gianelle @ PD.INFN
//-----------------------------------------------------------------------------

#include "PatFwdSerialization.h"

#include <stdexcept>
#include <string>

using namespace std;

//------------------
// utility functions
//------------------

template<typename T>
const uint8_t * copy(
    const uint8_t  * buffer,
    std::vector<T> & collection,
    size_t           count) {
  std::copy(
    (const T *)buffer,
    (const T *)buffer + count,
    std::back_inserter(collection));
  return buffer + sizeof(T) * count;
}

template<typename T>
uint8_t * copy(const std::vector<T> & collection, uint8_t * buffer) {
  return (uint8_t *)std::copy(collection.begin(), collection.end(), (T*)buffer);
}

template<typename T>
uint8_t * copy(const T* collection, int size, uint8_t * buffer) {
  return (uint8_t *)std::copy(collection, collection + size, (T*)buffer);
}

//------------------------------------
// PatFwdSerialization implementation
//------------------------------------

void PatFwdSerialization::cleanEvent(){

  m_event.noVeloTks = 0;  

  m_event.vt_id.clear();
  m_event.vt_x0.clear();
  m_event.vt_tx.clear();
  m_event.vt_y0.clear();
  m_event.vt_ty.clear();
  m_event.vt_z0.clear();  
  m_event.vt_qoverp.clear();

  m_event.noOTHits = 0;
  
  m_event.oth_LhcbID.clear();
  m_event.oth_channelID.clear();
  m_event.oth_xAtYEq0.clear();
  m_event.oth_zAtYEq0.clear();
  m_event.oth_yBegin.clear();
  m_event.oth_yEnd.clear();
  m_event.oth_weight.clear();
  m_event.oth_driftDistance.clear();
  m_event.oth_next.clear();
  m_event.oth_prev.clear();
 
}

void PatFwdSerialization::addVeloTk(LHCb::Track* veloTk, unsigned int id) {
  m_event.vt_id.push_back(id);
  m_event.vt_x0.push_back(veloTk->stateAt(LHCb::State::EndVelo)->x());
  m_event.vt_tx.push_back(veloTk->stateAt(LHCb::State::EndVelo)->tx());
  m_event.vt_y0.push_back(veloTk->stateAt(LHCb::State::EndVelo)->y());
  m_event.vt_ty.push_back(veloTk->stateAt(LHCb::State::EndVelo)->ty());
  m_event.vt_z0.push_back(veloTk->stateAt(LHCb::State::EndVelo)->z());
  m_event.vt_qoverp.push_back(veloTk->stateAt(LHCb::State::EndVelo)->qOverP());
  m_event.noVeloTks++;
}

void PatFwdSerialization::addHit(PatFwdHit* hit) {

    m_event.oth_LhcbID.push_back(hit->hit()->lhcbID().lhcbID());
    m_event.oth_channelID.push_back(LHCb::LHCbID(hit->hit()->lhcbID()).channelID());
    m_event.oth_xAtYEq0.push_back(hit->hit()->xAtYEq0());
    m_event.oth_zAtYEq0.push_back(hit->hit()->zAtYEq0());
    m_event.oth_yBegin.push_back(hit->hit()->yBegin());
    m_event.oth_yEnd.push_back(hit->hit()->yEnd());
    m_event.oth_weight.push_back(hit->hit()->weight());
    m_event.oth_driftDistance.push_back(hit->driftDistance());
    m_event.oth_next.push_back(hit->hasNext());
    m_event.oth_prev.push_back(hit->hasPrevious());
    m_event.noOTHits++;
}

void PatFwdSerialization::serializeEvent(Data & buffer) {

  // compute total size and allocate memory
  const size_t velotkSize = sizeof(int) + 6 * sizeof(float);
  const size_t velotksSize = m_event.noVeloTks * velotkSize + sizeof(int);
  
  const size_t othitSize = 4*sizeof(int) + 6*sizeof(float);
  const size_t othitsSize = m_event.noOTHits * othitSize + sizeof(int);

  buffer.resize(velotksSize + othitsSize);

  uint8_t * dst = (uint8_t *)&buffer[0];
  
  // serialize POD members
  *(int *)dst = m_event.noVeloTks;  dst += sizeof(int);

  dst = copy(m_event.vt_id,     dst);
  dst = copy(m_event.vt_x0,     dst);
  dst = copy(m_event.vt_tx,     dst);
  dst = copy(m_event.vt_y0,     dst);
  dst = copy(m_event.vt_ty,     dst);
  dst = copy(m_event.vt_z0,     dst);     
  dst = copy(m_event.vt_qoverp, dst);
  
  *(int *)dst = m_event.noOTHits;   dst += sizeof(int);
  
  dst = copy(m_event.oth_LhcbID,        dst);
  dst = copy(m_event.oth_channelID,     dst);
  dst = copy(m_event.oth_xAtYEq0,       dst);
  dst = copy(m_event.oth_zAtYEq0,       dst);
  dst = copy(m_event.oth_yBegin,        dst);
  dst = copy(m_event.oth_yEnd,          dst);
  dst = copy(m_event.oth_weight,        dst);  
  dst = copy(m_event.oth_driftDistance, dst);
  dst = copy(m_event.oth_next,          dst);
  dst = copy(m_event.oth_prev,          dst);
  
  assert(dst == &buffer[0] + buffer.size());
}

