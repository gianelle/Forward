#ifndef FWDKERNELS_CUH
#define FWDKERNELS_CUH

#include <iostream>
#include <vector>
#include "FwdData.cuh"

void setDetectorParameters();

__global__ void clearTracks( FwdTracks* );
__global__ void clearTracks( OutTracks* );
__global__ void find2DCandidates( VeloTracks*, FwdHitsEvContainer*, FwdTracks* );
__global__ void find2Dtracks( FwdHitsEvContainer*, FwdTracks* );
__global__ void find3Dtracks( FwdHitsEvContainer*, FwdTracks*, OutTracks* );
__global__ void parseInData( char*, int*, VeloTracks*, FwdHitsEvContainer* );
__global__ void removeGhost( OutTracks *input );

#endif
