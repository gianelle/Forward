#include "FwdTracking.cuh"
#include "FwdDefinitions.cuh"
#include "FwdData.cuh"
#include "FwdKernels.cuh"
#include <vector>
#include <cuda_profiler_api.h>

cudaError_t FwdTracking(const Batch & inBatch, std::vector<Data> & trackCollection) {

  int noevents = inBatch.size(); // number of events

  // device structures
  char       *dev_input = 0;

  VeloTracks         *d_VeloTracks;   
  FwdHitsEvContainer *d_FwdHits;
  
  dim3 grid;
  dim3 block;

  cudaError_t cudaStatus = cudaSuccess;

  CudaSafeCall(cudaSetDevice(0));
  CudaSafeCall(cudaProfilerStart());
 
  // array with the sizes of the events, used in the parse function
  std::vector<uint8_t> inData = *inBatch[0];
  int* bsize = (int*) malloc(sizeof(int)*noevents);
  bsize[0] = 0;
  for (int i = 0; i < noevents-1; ++i) {
    bsize[i+1] = bsize[i] + (*inBatch[i]).size();
    inData.insert(inData.end(), (*inBatch[i+1]).begin(), (*inBatch[i+1]).end());
  }
  int* dev_bsize = 0;  
  CudaSafeCall( cudaMalloc((void**)&dev_bsize, noevents*sizeof(int)));
  CudaSafeCall( cudaMemcpy(dev_bsize, &(bsize[0]), noevents*sizeof(int), cudaMemcpyHostToDevice) );  
  
  // total size in bytes of input data
  unsigned int tot = bsize[noevents-1] + (*inBatch[noevents-1]).size();    

  //printf("Input data: %d bytes for %d event(s)\n", tot, noevents);

  CudaSafeCall( cudaMalloc((void**)&dev_input, tot));
  CudaSafeCall( cudaMemcpy(dev_input, &(inData[0]), tot, cudaMemcpyHostToDevice));
  
  // Copy input file from host memory to GPU buffers
  CudaSafeCall( cudaMalloc(&d_FwdHits, noevents*sizeof(FwdHitsEvContainer)) );  
  CudaSafeCall( cudaMalloc(&d_VeloTracks, noevents*sizeof(VeloTracks)) );  

  // parse all events in parallel
  
  parseInData<<< noevents, 256 >>>(dev_input, dev_bsize, d_VeloTracks, d_FwdHits);
  CudaCheckError();
  // free memory for no more used data
  free(bsize);
  CudaSafeCall( cudaFree(dev_input) );
  CudaSafeCall( cudaFree(dev_bsize) );
/*
  FwdHitsEvContainer *h_FwdHits = new FwdHitsEvContainer;
  CudaSafeCall( cudaMemcpy(h_FwdHits, d_FwdHits, sizeof(FwdHitsEvContainer), cudaMemcpyDeviceToHost));

  h_FwdHits->debug();
*/
  // costants (?) parameters...
  setDetectorParameters();

  FwdTracks* d_FwdTracks;
  CudaSafeCall( cudaMalloc(&d_FwdTracks, noevents*sizeof(FwdTracks)));
  OutTracks* d_OutputTks;
  CudaSafeCall( cudaMalloc(&d_OutputTks, noevents*sizeof(OutTracks)) ); 
  
  clearTracks<<< noevents, 1 >>>( d_FwdTracks );
  CudaCheckError();

  clearTracks<<< noevents, 1 >>>( d_OutputTks );
  CudaCheckError(); 

  dim3 blocks( noevents, VELOTK );  
  
  find2DCandidates <<< blocks, 128 >>> ( d_VeloTracks, d_FwdHits, d_FwdTracks );
  CudaCheckError(); 
 /* 
  FwdTracks* h_FwdTracks = (FwdTracks*) malloc(sizeof(FwdTracks));
  CudaSafeCall( cudaMemcpy(h_FwdTracks, d_FwdTracks, sizeof(FwdTracks), cudaMemcpyDeviceToHost));
  for (int i=0; i < h_FwdTracks[0].size(); i++)
    printf("Candidates for track %d: %d\n", i, h_FwdTracks[0][i].size());
  */
  CudaSafeCall( cudaFree(d_VeloTracks) );
  
  find2Dtracks <<< blocks, MAXTRACKS >>>( d_FwdHits, d_FwdTracks );
  CudaCheckError();
  
  find3Dtracks <<< blocks, MAXTRACKS >>> ( d_FwdHits, d_FwdTracks, d_OutputTks );
  CudaCheckError();
  
  removeGhost <<< dim3(2*VELOTK, noevents), dim3(32, 32)  >>> ( d_OutputTks );
  CudaCheckError();
 
  OutTracks h_OutputTks[noevents];
  CudaSafeCall( cudaMemcpy(h_OutputTks, d_OutputTks, noevents*sizeof(OutTracks), cudaMemcpyDeviceToHost));

  for (int i = 0; i < noevents; ++i) {
    int noTracks = h_OutputTks[i].size();

    if ( noTracks >  0) {
      trackCollection[i].resize(noTracks * sizeof(OutTrack));
      OutTrack* solutionTracks = (OutTrack*)&trackCollection[i][0];
      int count = 0;
      for (int j = 0; j < noTracks; ++j)
        if ( h_OutputTks[i][j].valid )          
          solutionTracks[count++] = h_OutputTks[i][j];
      trackCollection[i].resize(count * sizeof(OutTrack));
      //printf("Ev %d: Fwd FINAL tracks found: %d\n", i, count);
    }
  }
  
  CudaSafeCall( cudaFree(d_OutputTks) ); 
  CudaSafeCall( cudaFree(d_FwdHits) );
  CudaSafeCall( cudaFree(d_FwdTracks) );

  
  //CudaSafeCall(cudaDeviceReset()); 
  CudaSafeCall( cudaProfilerStop() ); 
  
  return cudaStatus;
}


