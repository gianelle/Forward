#ifndef FWDDEFINITIONS_CUH
#define FWDDEFINITIONS_CUH


#include <iostream>

#define CUDA_ERROR_CHECK

#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError()    __cudaCheckError( __FILE__, __LINE__ )

inline void __cudaSafeCall( cudaError err, const char *file, const int line ) {
#ifdef CUDA_ERROR_CHECK
  if ( cudaSuccess != err ) {
    fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n",
             file, line, cudaGetErrorString( err ) );
    exit( -1 );
  }
#endif
  return;
}

inline void __cudaCheckError( const char *file, const int line ) {
#ifdef CUDA_ERROR_CHECK
  cudaError err = cudaGetLastError();
  if ( cudaSuccess != err ) {
    fprintf( stderr, "cudaCheckError() failed at %s:%i : %s\n",
             file, line, cudaGetErrorString( err ) );
    exit( -1 );
  }

  // More careful checking. However, this will affect performance.
  // Comment away if needed.
  /*
  err = cudaDeviceSynchronize();
  if( cudaSuccess != err ) {
    fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
              file, line, cudaGetErrorString( err ) );
    exit( -1 );
  }
  */
#endif
  return;
}


#ifdef UseRealMap
  const double m_zMagnetParams[] = {5208.05, 318.502, -1223.87, 9.80117e-06, -304.272};
  const double m_xParams[] = {17.5815, -5.94803};
  const double m_yParams[] = {-979.0, -0.684947};
  const double m_momentumParams[] = {1.21174, 0.634127, -0.242116, 0.412728, 2.82916, -20.6599};
#else
  const double m_zMagnetParams[] = {5199.31, 334.725, -1283.86, 9.59486e-06, -413.281};
  const double m_xParams[] = {16.8238, -6.35556};
  const double m_yParams[] = {-970.89, -0.686028};
  const double m_momentumParams[] = {1.21909, 0.627841, -0.235216, 0.433811, 2.92798, -21.3909};
#endif



// From StateParameters.h 
// all are in Gaudi::Units::mm
#define ZEndT       9410
#define ZBegT       7500  
#define ZMidT       8520.  
#define ZMidTT      2485  
#define ZEndTT      2700  
#define ZEndVelo     770  
#define zAfterVelo  1640

#define STATIONS   3
#define LAYERS     4
#define REGIONS    6  // 2 OT + 4 IT
#define PLANES     STATIONS*LAYERS


// TT costants from PatForwardTool
// for layer u, v theta is 5 degrees
#define c_sinT                0.0871563
// nominal slope of the plane => the rotation angle of the beamline respect the horizontal plane
#define c_dzDy                0.00360102
#define c_dxDy                0.0874892
#define c_minPt               0.5 // 0.08 // * Gaudi::Units::GeV
#define c_minMomentum         3. // 1.   // * Gaudi::Units::GeV
#define c_magnetKickParams0   1.255 // * Gaudi::Units::GeV
#define c_magnetKickParams1   0.175 // * Gaudi::Units::GeV
#define c_yCompatibleTol     10     // * Gaudi::Units::mm
#define c_minOTDrift         -0.3   // * Gaudi::Units::mm 
#define c_maxOTDrift          2.5    // * Gaudi::Units::mm 
#define c_WrongSignPT      2000.0    // * Gaudi::Units::MeV
#define c_minXPlanes          5
#define c_maxSpreadSlopeX     0.0075 // 0.011
#define c_maxSpreadX          0.65 // 0.6
#define c_maxSpreadSlopeY    70.
#define c_maxSpreadY          1.5 
#define c_maxXCandidateSize  30 //50
#define c_minPlanes           9
#define c_maxChi2            36 // 40
#define c_xMagnetTol          3.
#define c_xMagnetTolSlope    40.
#define c_maxChi2Track       40 
#define c_centerOTYSize     100  // * Gaudi::Units::mm
#define c_maxDeltaY          30.
#define c_maxDeltaYSlope    300.
#define c_minHits            12 
#define c_minOTHits          14
#define c_yCompatibleTolFinal 1. // 10. // * Gaudi::Units::mm

// others parameters (?)
// True if using measured map
#define c_useRealMap              true
// scale factor including polarity and current
#define c_signedRelativeCurrent   -1.


// max hits per track
#define FWDHITS   140  //115
// max hits per region
#define MAXHITXREG 256 // 600
// max hits in the ref list -> see find2Dtracks
#define MaxHitXRef 1024
// total numbers of regions
#define NoREGIONS STATIONS*LAYERS*REGIONS
// max hits per event
#define MAXHITSXEV NoREGIONS*MAXHITXREG
// max candidate tracks
#define MAXTRACKS 60

#define VELOTK 512

#define UseRealMap true


#endif
