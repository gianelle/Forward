#include "GpuHandler/IGpuHandler.h"

class FwdCudaHandler : public IGpuHandler
{
    virtual void operator() (
        const Batch & batch,
        Alloc         allocResult,
        AllocParam    allocResultParam);
};
