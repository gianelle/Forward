#ifndef FWDTRACKING_CUH
#define FWDTRACKING_CUH

#include <stdint.h>
#include <vector>
#include "cuda_runtime.h"

typedef std::vector<uint8_t>     Data;
typedef std::vector<const Data*> Batch;

cudaError_t FwdTracking(const Batch&, std::vector<Data>&);



#endif
