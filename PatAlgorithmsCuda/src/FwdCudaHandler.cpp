#include "FwdCudaHandler.h"
#include "FwdTracking.cuh"
#include <algorithm>
#include <vector>
#include <ctime>
#include <iostream>

using namespace std;

DECLARE_COMPONENT(FwdCudaHandler)

void FwdCudaHandler::operator() (
    const Batch & batch,
    Alloc         allocResult,
    AllocParam    allocResultParam) {

  vector<Data> trackCollection;

  trackCollection.resize(batch.size());
  
  FwdTracking(batch, trackCollection);

  for (int i = 0; i < trackCollection.size(); ++i) {
    uint8_t * buffer = allocResult(i, trackCollection[i].size(), allocResultParam);
    copy(trackCollection[i].begin(), trackCollection[i].end(), buffer);
  }
  
}
