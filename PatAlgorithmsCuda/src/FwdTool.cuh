#ifndef FWDTOOL_CUH
#define FWDTOOL_CUH 1

#include "FwdDefinitions.cuh"
#include "stdio.h"

// template for a std::vector-like class handling vectors of fixed dimension
template<class A, unsigned int N>
class PVector {
  public:
    // constructor
    __host__ __device__ PVector(): m_size (0) {}

    __device__ PVector(const PVector& v) {
      this->m_size = v.m_size;
      for (int i = 0; i < this->m_size; i++)
        this->m_items[i] = v.m_items[i];
    }

    __device__ PVector& operator=(const PVector& v) {
      this->m_size = v.m_size;
      for (int i = 0; i < this->m_size; i++)
        this->m_items[i] = v.m_items[i];
      return *this;
    }

    // members 
    __host__ __device__ A& operator[] (unsigned int n) { return m_items[n]; }
    __host__ __device__ unsigned int size() const { return m_size; } 
    __device__ A& front() { return m_items[0]; }
    __device__ A& back()  { return m_items[m_size-1]; }
    __host__ __device__ void clear() { m_size = 0; }
    __device__ bool empty() const { return m_size == 0; }
    __device__ bool full() const { return m_size >= N; }
    __device__ void forceSize( int n) { m_size = n; } 
    __device__ A* elements() { return m_items; }

    // swap elements
    __device__ void swap(int i, int j) {
      if ( (i >= m_size) || (j >= m_size) ) {
        printf("ERROR! Index overflow!\n");
        return;
      }  
      A tmp;
      tmp = m_items[i];
      m_items[i] = m_items[j];
      m_items[j] = tmp;    
      return;
    }
    
    // add element and increase size
    __device__ void add(const A& a) {
      if (m_size < N)
	      m_items[m_size++] = a;
      //else
	    //  printf("ERROR: PVector add: overflow detected (size= %i) \n", N);
    }
    
    // atomicadd element, returns the index of the added element
    __device__ int atomicadd(const A& a) {
      int iold = atomicAdd (&m_size, 1);
      if (iold < N)
	      m_items[iold] = a;
      else {
	    //  printf("ERROR: PVector atomicadd: overflow detected (size = %i) \n", N);
	      m_size = N;
	    }
	    return iold;  
    }

    // erase an element from vector
    __device__ void erase (unsigned int index) {
      for ( int i = index; i < m_size-1; ++i)
	      m_items[i] = m_items[i+1];
      // resize 
      m_size--;
    }

    A m_items[N];
    unsigned int m_size;

};

__device__ static float atomicMax(float* address, float val)
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fmaxf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}

__device__ static float atomicMin(float* address, float val)
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fminf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}


//=============================================================================
// Simple class to fit a parabola

class FitLine {
  public:
    /// Standard constructor
    __device__ FitLine( double z = 0., double x = 0., double w = 0. ) {
      //== First point
      const double dz = 1.e-3 * z;  // use small numbers
      m_mat[0] = w;
      m_mat[1] = w * dz;
      m_mat[2] = w * dz * dz;
      m_rhs[0] = w * x;
      m_rhs[1] = w * x * dz;
      m_sol[0] = 0.;
      m_sol[1] = 0.;
    }

    __device__ ~FitLine( ) {}; ///< Destructor

    __device__ void addPoint( double z, double x, double w ) {
      const double dz = 1.e-3*z;
      m_mat[0] += w;
      m_mat[1] += w * dz;
      m_mat[2] += w * dz * dz;
      m_rhs[0] += w * x;
      m_rhs[1] += w * x * dz;
    }

    /// return false if matrix is singular
    __device__ bool solve() {
      
      // det = 0 => singular
      double det = m_mat[0]*m_mat[2] - m_mat[1]*m_mat[1];
      if ( det == 0 || m_mat[0] == 0) return false; 
       
      // Ok we can do better... next time!  
  
      m_sol[1] = ( m_mat[0]*m_rhs[1] - m_mat[1]*m_rhs[0] ) / det;
      m_sol[0] = ( m_rhs[0] - m_mat[1] * m_sol[1] ) / m_mat[0];
      
      m_sol[1] *= 1.e-3;

      return true;

    }

    __device__ double ax() const { return m_sol[0]; }
    __device__ double bx() const { return m_sol[1]; }
    __device__ double z0() const { return 1.e3 * m_mat[1] / m_mat[0]; }

  protected:

  private:
    double m_mat[3]; /// matrix M in Mx = b
    double m_rhs[2]; /// vector b in Mx = b
    double m_sol[2]; /// (solution) vector x in Mx = b
  };



//=============================================================================
// Simple class to fit a parabola

class FitParabola {
  public:
    /// Standard constructor
    __device__ FitParabola( double z = 0., double x = 0., double w = 0. ) {
      //== First point, used to constraint the tangent at z=0 to go to this point.
      const double dz = 1.e-3 * z;  // use small numbers
      m_mat[0] = w;
      m_mat[1] = w * dz;
      m_mat[2] = w * dz * dz;
      m_mat[3] = 0.;
      m_mat[4] = 0.;
      m_mat[5] = 0.;
      m_rhs[0] = w * x;
      m_rhs[1] = w * x * dz;
      m_rhs[2] = 0.;
      m_sol[0] = 0.;
      m_sol[1] = 0.;
      m_sol[2] = 0.;
    }

    __device__ ~FitParabola( ) {}; ///< Destructor

    __device__ void addPoint( double z, double x, double w ) {
      const double dz = 1.e-3*z;
      m_mat[0] += w;
      m_mat[1] += w * dz;
      m_mat[2] += w * dz * dz;
      m_mat[3] += w * dz * dz;
      m_mat[4] += w * dz * dz * dz;
      m_mat[5] += w * dz * dz * dz * dz;
      m_rhs[0] += w * x;
      m_rhs[1] += w * x * dz;
      m_rhs[2] += w * x * dz * dz;
    }

    /// return false if matrix is singular
    __device__ bool solve() {
      // det = 0 => singular
      double det = m_mat[0]*m_mat[2]*m_mat[5] + 2*m_mat[1]*m_mat[3]*m_mat[4] - 
                  ( m_mat[2]*m_mat[3]*m_mat[3] + m_mat[0]*m_mat[4]*m_mat[4] + 
                  m_mat[1]*m_mat[1]*m_mat[5] );
      if ( det == 0 || m_mat[0] == 0) return false; 
      
      // Ok we can do better... next time!  

      // LDL(t) decomposition
      double d[3];
      double l[3];
      
      d[0] = m_mat[0];
      l[0] = m_mat[1] / d[0];  
      d[1] = m_mat[2] - l[0]*l[0]*d[0];
      if ( d[1] == 0 ) return false;
      l[1] = m_mat[3] / d[0];
      l[2] = (m_mat[4] - l[1]*d[0]*l[0]) / d[1];
      d[2] = m_mat[5] - (l[1]*l[1]*d[0] + l[2]*l[2]*d[1]);
      
      // solve Lz = b
      double z[3];
      z[0] = m_rhs[0];
      z[1] = m_rhs[1] - l[0]*m_rhs[0];
      z[2] = m_rhs[2] - l[1]*z[0] - l[2]*z[1];

      // solve L(t)x = y = D^(-1)z
      m_sol[2] = z[2]/d[2];
      m_sol[1] = z[1]/d[1] - l[2]*m_sol[2];
      m_sol[0] = z[0]/d[0] - l[0]*m_sol[1] - l[1]*m_sol[2];

      m_sol[1] *= 1.e-3;
      m_sol[2] *= 1.e-6;

      return true;
    }

    __device__ double ax() const { return m_sol[0]; }
    __device__ double bx() const { return m_sol[1]; }
    __device__ double cx() const { return m_sol[2]; }

  private:
    // Mx = b
    double m_mat[6]; /// matrix M 
    double m_rhs[3]; /// (right hand side) vector b
    double m_sol[3]; /// (solution) vector x
};



#endif // FWDTOOL_H
