#ifndef FWDHIT_H
#define FWDHIT_H 1

// Include files
#include "FwdDefinitions.cuh"
#include "FwdTool.cuh"

template <typename t>
__host__ __device__ void swap (t& x, t& y) {
  t temp = x;
  x = y;
  y = temp;
  return;
}



class FwdHit {
  private:
    __device__ unsigned set(bool b, unsigned mask, unsigned val) { 
      return ( val & ~mask ) | ( unsigned(-b) & mask ) ; 
    }
    __device__ void set(bool b, unsigned mask) { 
      m_flags = ( m_flags & ~mask ) | ( unsigned(-b) & mask ) ; 
    }


  public:

    // Default constructor
    __device__ FwdHit() :
    //  m_index(-1),
      m_data(-1),
      m_driftDistance(0),
      m_x(0),
      m_z(0),
      m_projection(99999),
    //  m_region(-1),
    //  m_layer(-1),
      m_flags(0u)
    {};
    
    __device__ FwdHit( int idx, float dd, float x, float z, int s, int l, int r ) :
      //m_index(idx),
      m_driftDistance(dd),
      m_x(x),
      m_z(z),
      m_projection(99999)
      //m_region(r),
      //m_layer(l)
     { m_flags = 4 * s + l; m_data = 100*idx+10*l+r; }  
    
    __device__ FwdHit& operator=(const FwdHit& v) {
      this->m_data = v.m_data;
      //this->m_index = v.m_index;
      this->m_x = v.m_x;
      this->m_z = v.m_z;
      this->m_driftDistance = v.m_driftDistance;
      this->m_projection = v.m_projection;
      //this->m_region = v.m_region;
      //this->m_layer = v.m_layer;
      this->m_flags = v.m_flags;
      
      return *this;
    }

    /// Desctructor
    __device__ ~FwdHit() { }

    __device__ void init(int idx, float dd, float x, float z, int s, int l, int r ) {
      //m_index = idx;
      m_driftDistance = dd;
      m_x = x; 
      m_z = z;
      //m_region = r;
      //m_layer = l;
      m_flags = 4 * s + l;
      m_data = 100*idx+10*l+r;
    }  

    // Accessors
    __device__ int   index()          const { return m_data/100; }
    __device__ float driftDistance()  const { return m_driftDistance; }
    __device__ float x()              const { return m_x; }
    __device__ float z()              const { return m_z; }
    __device__ float projection()     const { return m_projection; }
    __device__ int region()           const { return m_data%10; }
    __device__ int layer()            const { return (m_data/10)%10; }

    __device__ int  rlAmb()           const { return ( m_flags & 0x4000u ) ? -1  :
                                        ( m_flags & 0x2000u ) ? +1  : 0 ; }
    __device__ bool hasPrevious()     const { return   m_flags & 0x1000u; }
    __device__ bool hasNext()         const { return   m_flags & 0x0800u; }
    __device__ bool isIgnored()       const { return   m_flags & 0x0400u; }
    __device__ bool isUsed()          const { return   m_flags & 0x0200u; }
    __device__ bool isSelected()      const { return   m_flags & 0x0100u; }
    __device__ int  planeCode()       const { return   m_flags & 0x00ffu; }

    // Setters
    __device__ void setIndex( int idx )                        { m_data = 100*idx + m_data%100; }  
    __device__ void setDriftDistance( float driftDistance )    { m_driftDistance = driftDistance; }
    __device__ void setX( float x )                            { m_x = x; }
    __device__ void setZ( float z )                            { m_z = z; }
    __device__ void setProjection( float proj )                { m_projection = proj; }

    __device__ void setRlAmb( int rl )                       { m_flags = set( rl != 0, 0x2000u,
                                                                set( rl < 0, 0x4000u, m_flags)); }
    __device__ void setHasPrevious( bool hasPrevious )       { set(hasPrevious, 0x1000u ); }
    __device__ void setHasNext( bool hasNext )               { set(hasNext,     0x0800u ); }
    __device__ void setIgnored( bool isIgnored )             { set(isIgnored,   0x0400u ); }
    __device__ void setIsUsed(bool isUsed)                   { set(isUsed,      0x0200u ); }
    __device__ void setSelected( bool isSelected )           { set(isSelected,  0x0100u ); }


    __device__ bool isOT() const { return ( region() < 2 ); }

    __device__ float dxDy( ) const { 
      return ( layer() == 1 ) ? c_dxDy : ( layer() == 2 ) ? -c_dxDy : 0; 
    }

    __device__ float dzDy( ) const { return c_dzDy; }

    __device__ float sinT() const { 
      return ( layer() == 1 ) ? -c_sinT : ( layer() == 2 ) ? c_sinT : 0; 
    }

    __device__ void debug(int event, int vt) {
      printf("[%d, %d] DEBUG  [%d] Z %f Xp %f X %f L %d R %d Prev %d Next %d Drift %f\n",
            event, vt, index(), m_z, m_projection, m_x, layer(), region(), hasPrevious(), 
            hasNext(), m_driftDistance);  
    }

  private:
//    int    m_index; // global index in the hit's container
    int    m_data;
    float  m_driftDistance; 
    float  m_x; 
    float  m_z; 
    float  m_projection;   
//    int    m_region; // to compute isOT
//    int    m_layer;  // to compute dxDy and sinT
    // it contains: RlAmb, HasPrev and HasNext, 
    // Ignored, IsUsed, Selected and planeCode
    unsigned m_flags;
  
};

// Hits's container per event
class FwdHitsEvContainer {

  private:

    /// Offsets of bitfield of OTchannelID
    enum channelIDBitsOT{tdcTimeBitsOT     = 0,
                       strawBitsOT         = 8,
                       moduleBitsOT        = 16,
                       quarterBitsOT       = 20,
                       layerBitsOT         = 22,
                       stationBitsOT       = 24};

    /// Bitmasks for bitfield of OTchannelID
    enum channelIDMasksOT{tdcTimeMaskOT     = 0xffL,
                        strawMaskOT         = 0xff00L,
                        moduleMaskOT        = 0xf0000L,
                        quarterMaskOT       = 0x300000L,
                        layerMaskOT         = 0xc00000L,
                        stationMaskOT       = 0x3000000L,
                        uniqueLayerMaskOT   = layerMaskOT + stationMaskOT,
                        uniqueQuarterMaskOT = quarterMaskOT + layerMaskOT + stationMaskOT,
                        uniqueModuleMaskOT  = moduleMaskOT + quarterMaskOT + layerMaskOT + stationMaskOT,
                        uniqueStrawMaskOT   = strawMaskOT + moduleMaskOT + quarterMaskOT + layerMaskOT + stationMaskOT
                       };
    
    /// Offsets of bitfield of ITchannelID
    enum channelIDBitsIT{stripBitsIT         = 0,
                       sectorBitsIT          = 10,
                       detRegionBitsIT       = 15,
                       layerBitsIT           = 18,
                       stationBitsIT         = 21,
                       typeBitsIT            = 23};

    /// Bitmasks for bitfield of ITchannelID
    enum channelIDMasksIT{stripMaskIT         = 0x3ffL,
                        sectorMaskIT          = 0x7c00L,
                        detRegionMaskIT       = 0x38000L,
                        layerMaskIT           = 0x1c0000L,
                        stationMaskIT         = 0x600000L,
                        typeMaskIT            = 0x1800000L,
                        uniqueLayerMaskIT     = layerMaskIT + stationMaskIT,
                        uniqueDetRegionMaskIT = detRegionMaskIT + layerMaskIT + stationMaskIT,
                        uniqueSectorMaskIT    = sectorMaskIT + detRegionMaskIT + layerMaskIT + stationMaskIT
                       };

    
    // given (s, l, r) coords returns region's global index
    __host__ __device__ int getRegId(int s, int l, int r) {
      return ( s * LAYERS * REGIONS + l * REGIONS + r );
    }    

  public:
  
    // constructor
    __host__ __device__ FwdHitsEvContainer() { reset(); }
    
    // destructor
    __device__ ~FwdHitsEvContainer() {}  

    __host__ __device__ void reset() {
       // reset regions's counters
      for (int s = 0; s < NoREGIONS; ++s) {
        m_nohits[s] = 0;
        m_zmin[s] = 99999.0;
        m_ymin[s] = 99999.0;
        m_zmax[s] = -99999.0;
        m_ymax[s] = -99999.0;
      }      
      m_nohits[NoREGIONS] = 0;  
      
    }
    __device__ int getGlobalIndex(int s, int l, int r, int i) {
      return m_nohits[getRegId(s, l, r)] + i;
    }

    // dd = 0 => IT hit 
    // ARG: GlobalIdx, LHCbID, ClusterID, hasNext, hasPrevious, yBegin, yEnd, xAtYEq0, zAtYEq0, driftDistance, weight
    __device__ void addHit(int gidx, int id, int cid, bool hn, bool hp, float yb, float ye, float x, float z, float dd, float w)  {
      if ( gidx >= MAXHITSXEV ) {
        printf("ERROR: data overflow; we exceed max limit for hits in event\n");
        return;
      }
      
      int s, l, r;
      if ( dd == 0 ) { // IT
        s = (unsigned int)((cid & stationMaskIT) >> stationBitsIT) - 1;
        l = (unsigned int)((cid & layerMaskIT) >> layerBitsIT) - 1;
        r = (unsigned int)((cid & detRegionMaskIT) >> detRegionBitsIT) - 1 + 2;
      } else { // OT
        s = (unsigned int)((cid & stationMaskOT) >> stationBitsOT) - 1;
        l = (unsigned int)((cid & layerMaskOT) >> layerBitsOT);
        r = (unsigned int)((cid & quarterMaskOT) >> quarterBitsOT) / 2;
      }
      
      int idx = getRegId(s, l, r);
      int iold = atomicAdd(&m_nohits[idx+1], 1);

      m_lhcbId[gidx]        = id;
      m_channelId[gidx]     = cid;
      m_hasNext[gidx]       = hn;
      m_hasPrev[gidx]       = hp; 
      m_yBegin[gidx]        = yb;
      m_yEnd[gidx]          = ye;
      m_driftDistance[gidx] = dd;
      m_xAtYEq0[gidx]       = x;
      m_zAtYEq0[gidx]       = z;
      m_weight[gidx]        = w; 
  
      atomicMin(&m_zmin[idx], z);
      atomicMax(&m_zmax[idx], z);
      atomicMin(&m_ymin[idx], min( yb, ye ));
      atomicMax(&m_ymax[idx], max( yb, ye ));

      return;
    }
    // must be called after the adding of all the hits
    __device__ void setnumhits() {
      for (int i = 2; i < NoREGIONS+1; i++ )
        m_nohits[i] += m_nohits[i-1];
    }

    // setters
    // global index used as parameter
    __device__ void setYBegin(int idx, float yb) { m_yBegin[idx] = yb; }
    __device__ void setYEnd(int idx, float ye) { m_yEnd[idx] = ye; }
    __device__ void setDriftDistance(int idx, float dd) { m_driftDistance[idx] = dd; }
    __device__ void setxAtYEq0(int idx, float xy) { m_xAtYEq0[idx] = xy; }
    __device__ void setzAtYEq0(int idx, float zy) { m_zAtYEq0[idx] = zy; }
    __device__ void setWeight(int idx, int wgh) { m_weight[idx] = wgh; }
    
    __device__ void setYBegin(int sta, int lay, int reg, int i, float x) {
      setYBegin(getGlobalIndex(sta, lay, reg, i), x);
    }
    __device__ void setYEnd(int sta, int lay, int reg, int i, float z) {
      setYEnd(getGlobalIndex(sta, lay, reg, i), z);
    }
    __device__ void setDriftDistance(int sta, int lay, int reg, int i, float dd) {
      setDriftDistance(getGlobalIndex(sta, lay, reg, i), dd);
    }
    __device__ void setxAtYEq0(int sta, int lay, int reg, int i, float xy) {
      setxAtYEq0(getGlobalIndex(sta, lay, reg, i), xy);
    }    
    __device__ void setzAtYEq0(int sta, int lay, int reg, int i, float zy) {
      setzAtYEq0(getGlobalIndex(sta, lay, reg, i), zy);
    }       
    __device__ void setWeight(int sta, int lay, int reg, int i, float wgh) {
      setWeight(getGlobalIndex(sta, lay, reg, i), wgh);
    }    

    // getters
    __device__ FwdHit getFwdHit(int index) {
      FwdHit hit;
      if ( index >= m_nohits[NoREGIONS] ) {
        printf("Error! Hit not exists\n");
        return hit;
      }        
      
      int sta, lay, reg;
      int cid = m_channelId[index];
      if ( m_driftDistance[index] == 0 ) { // IT
        sta = (unsigned int)((cid & stationMaskIT) >> stationBitsIT) - 1;
        lay = (unsigned int)((cid & layerMaskIT) >> layerBitsIT) - 1;
        reg = (unsigned int)((cid & detRegionMaskIT) >> detRegionBitsIT) - 1 + 2;
      } else { // OT
        sta = (unsigned int)((cid & stationMaskOT) >> stationBitsOT) - 1;
        lay = (unsigned int)((cid & layerMaskOT) >> layerBitsOT);
        reg = (unsigned int)((cid & quarterMaskOT) >> quarterBitsOT) / 2;
      }
      
      hit.init( index, m_driftDistance[index],
                m_xAtYEq0[index], m_zAtYEq0[index],
                sta, lay, reg);
      hit.setHasNext( m_hasNext[index] );
      hit.setHasPrevious( m_hasPrev[index] );                 
      return hit;
    }  
    
    __device__ FwdHit getFwdHit(int sta, int lay, int reg, int i) {
      return getFwdHit( getGlobalIndex(sta, lay, reg, i) ); 
    }
    
    // getters
    // global index used as parameter
    __device__ int getLhcbID(int idx) const { return m_lhcbId[idx]; }
    __device__ float yBegin(int idx) const { return m_yBegin[idx]; }      
    __device__ float yEnd(int idx) const { return m_yEnd[idx]; } 
    __device__ float driftDistance(int idx) const { return m_driftDistance[idx]; } 
    __device__ float xAtYEq0(int idx) const { return m_xAtYEq0[idx]; }    
    __device__ float zAtYEq0(int idx) const { return m_zAtYEq0[idx]; }
    __device__ double weight(int idx) const { return m_weight[idx]; }

    // specify all the "coords"
    __device__ float yBegin(int sta, int lay, int reg, int i) {
      return yBegin(getGlobalIndex(sta, lay, reg, i));
    }  
    __device__ float yEnd(int sta, int lay, int reg, int i)  {
      return yEnd(getGlobalIndex(sta, lay, reg, i));
    }
    __device__ float driftDistance(int sta, int lay, int reg, int i) {
      return driftDistance(getGlobalIndex(sta, lay, reg, i));
    } 
    __device__ float xAtYEq0(int sta, int lay, int reg, int i) {
      return xAtYEq0(getGlobalIndex(sta, lay, reg, i));
    }
    __device__ float zAtYEq0(int sta, int lay, int reg, int i) {
      return zAtYEq0(getGlobalIndex(sta, lay, reg, i));
    }      
    __device__ double weight(int sta, int lay, int reg, int i) {
      return weight(getGlobalIndex(sta, lay, reg, i));
    }  
    
    // return the ( zmax - zmin ) / 2 of the hits in the given region
    __device__ float zRegion(int sta, int lay, int reg) {
      int z = getRegId(sta, lay, reg);
      return ( m_zmax[z] + m_zmin[z] ) / 2;
    }
    // Check if a given y value is compatible (inside) the hit region, to within a given tolerance
    __device__ bool isYCompatible(int gidx, const float y, const float tol ) { 
      return fabs( y - 0.5*(yBegin(gidx) + yEnd(gidx)) ) < 0.5*fabs(yBegin(gidx) - yEnd(gidx)) + tol;
    }
    // Check if a given y value is compatible (inside) the all region, to within a given tolerance
    __device__ bool isYCompatible(int s, int l, int r, const float y, const float tol ) { 
      int ridx = getRegId(s, l, r);
      return m_ymin[ridx] - tol <= y && y <= m_ymax[ridx] + tol ; 
    }
    
    // counters

    // total number of hits in the region
    __host__ __device__ int numHits(int station, int layer, int region) {
      return ( m_nohits[ getRegId(station, layer, region) + 1 ] -
                m_nohits[ getRegId(station, layer, region) ] );   
    }
    
    // sort all hits in all regions NB this could be done in //
    __device__ void sortAllHits() {
      for (int s = 0; s < STATIONS; ++s)
        for (int l = 0; l < LAYERS; ++l)
          for (int r = 0; r < REGIONS; ++r)  
            if ( numHits(s, l, r) > 1 ) sortHits(s, l, r);
    }  
    
    // sort hits inside a given region    
    __device__ void sortHits(int s, int l, int r) {
      int idx;
   
      int ridx = getRegId(s, l, r);
      for (int i = m_nohits[ridx]; i < m_nohits[ridx+1]-1; ++i) {
        idx = i;
        for (int j = i + 1; j < m_nohits[ridx+1]; ++j)
          if ( m_xAtYEq0[j] < m_xAtYEq0[idx] ) idx = j;
        if ( i != idx ) {
          swap(m_lhcbId[i], m_lhcbId[idx]);
          swap(m_channelId[i], m_channelId[idx]);
          swap(m_xAtYEq0[i], m_xAtYEq0[idx]);
          swap(m_zAtYEq0[i], m_zAtYEq0[idx]);          
          swap(m_yBegin[i], m_yBegin[idx]);
          swap(m_yEnd[i], m_yEnd[idx]);          
          swap(m_driftDistance[i], m_driftDistance[idx]);
          swap(m_weight[i], m_weight[idx]);
          swap(m_hasNext[i], m_hasNext[idx]);
          swap(m_hasPrev[i], m_hasPrev[idx]);
        }
      }
    }
    
    __host__ __device__ void debug() {  
      printf("DEBUG: Structure has %d hits\n", m_nohits[NoREGIONS]);
      for (int i=0; i <NoREGIONS+1; i++)
        printf("Region %d has %d hits\n", i, m_nohits[i]);
      for (int s = 0; s < STATIONS; ++s)
        for (int l = 0; l < LAYERS; ++l)
          for (int r = 0; r < REGIONS; ++r) {
            int ridx = getRegId(s,l,r);
            printf("DEBUG: We have %d hits in region (%d, %d, %d) -> %d:\n",  numHits(s,l,r), s, l, r, ridx);
            printf("DEBUG: \t index - LhcbID - channelID\t (x, z)_(AT y=0) \t\t (yBegin,yEnd) \t\t weight, driftDistance \t\t (Next, Prev)\n");
            for (int i = m_nohits[ridx]; i < m_nohits[ridx+1]; ++i)
              printf("DEBUG: \t %d - %d - %d\t (%f, %f) \t (%f, %f) \t (%f, %f) \t (%d, %d)\n", i, m_lhcbId[i], m_channelId[i], m_xAtYEq0[i], 
                      m_zAtYEq0[i], m_yBegin[i], m_yEnd[i], m_weight[i], m_driftDistance[i], m_hasNext[i], m_hasPrev[i]);
            printf("\nDEBUG: zmin = %f, zmax = %f, ymin = %f, ymax = %f\n", m_zmin[ridx], m_zmax[ridx], m_ymin[ridx], m_ymax[ridx]);
          }  
    }

  private: 

    // number of hits per single region
    // Given a station S, a layer L, a region R 
    // index = S * (LAYERS*REGIONS) + L * (REGIONS) + R
    int m_nohits[NoREGIONS+1];
      
    // hit parameters
    // do we need others parameters?
    // BTW if you add parameters change also the sortHits function
    int   m_lhcbId[MAXHITSXEV];
    int   m_channelId[MAXHITSXEV];
    bool  m_hasNext[MAXHITSXEV];
    bool  m_hasPrev[MAXHITSXEV];    
    float m_xAtYEq0[MAXHITSXEV];
    float m_zAtYEq0[MAXHITSXEV];
    float m_yBegin[MAXHITSXEV];
    float m_yEnd[MAXHITSXEV];
    float m_driftDistance[MAXHITSXEV];
    float m_weight[MAXHITSXEV];
    
    // region parameters
    float  m_zmin[NoREGIONS];
    float  m_zmax[NoREGIONS];
    float  m_ymin[NoREGIONS];
    float  m_ymax[NoREGIONS];
};

/// Auxiliary structures
//--------------------------------------
// Definition of FwdHit container class
//--------------------------------------

struct VeloTrack {
  int   id;
  float x0;
  float y0;
  float z0;
  float tx;
  float ty;
  float qoverp;
};

struct OutTrack {
// output tracks
  int   vtid; // velo track id
  // fit parameters
  double ax;
  double bx;
  double cx;
  double dx;
  double ay;
  double by;
  // "dof" values
  double chi2xdof;
  double ndof;
  // quality
  double quality;
  // lhcbID hits
  int   HitsNum;
  int   Hits[40];
  bool  valid;
};


typedef PVector<OutTrack, 2*VELOTK> OutTracks;

  /** @class FwdTrack FwdTrack.cuh
   *  Forward track. Basically, the Velo track, list of TCoord
   *  and T-station track parameters
   *
   */

class FwdTrack {
  
  public:
    /// Standard constructor

    __device__ FwdTrack( ) :
      m_vtid(0),
      m_x0(0), 
      m_y0(0),
      m_z0(0),
      m_tx(0),
      m_ty(0),
      m_qoverp(0.0),
      m_ignore(true),
      m_numHits(0),
      m_quality(0.0),
      m_ax(0.0),
      m_bx(0.0),
      m_cx(0.0),
      m_dx(0.0), 
      m_ay(0.0),
      m_by(0.0),
      m_cosAfter(1.0),
      m_zMagnet(0.0),
      m_chi2PerDoF(0.0),
      m_nDoF(0)  
     { }
    
    __device__ FwdTrack( const VeloTrack tr ) { 
      init( tr ); }
      
    __device__ FwdTrack( const VeloTrack tr, FwdHit* fhits, int size) { 
      init( tr );
      for (int i=0; i < size; ++i)
        this->m_hits[i] = fhits[i]; 
      m_numHits = size;
    }  
    
    
    __device__ FwdTrack(const FwdTrack& f) {
      this->m_vtid   = f.m_vtid;
      this->m_x0     = f.m_x0;
      this->m_y0     = f.m_y0;
      this->m_z0     = f.m_z0;
      this->m_tx     = f.m_tx;
      this->m_ty     = f.m_ty;
      this->m_qoverp = f.m_qoverp;
      this->m_ignore = f.m_ignore;
      this->m_numHits = f.m_numHits;
      for (int i=0; i < f.m_numHits; ++i)
        this->m_hits[i] = f.m_hits[i];
      this->m_quality = f.m_quality;  
      this->m_ax     = f.m_ax;
      this->m_bx     = f.m_bx;
      this->m_cx     = f.m_cx;
      this->m_dx     = f.m_dx;
      this->m_ay     = f.m_ay;
      this->m_by     = f.m_by;
      this->m_cosAfter = f.m_cosAfter;
      this->m_chi2PerDoF = f.m_chi2PerDoF;
      this->m_nDoF   = f.m_nDoF;
      this->m_zMagnet = f.m_zMagnet;
    }

    __device__ FwdTrack& operator=(const FwdTrack& f) {
      this->m_vtid   = f.m_vtid;
      this->m_x0     = f.m_x0;
      this->m_y0     = f.m_y0;
      this->m_z0     = f.m_z0;
      this->m_tx     = f.m_tx;
      this->m_ty     = f.m_ty;
      this->m_qoverp = f.m_qoverp;
      this->m_ignore = f.m_ignore;
      this->m_numHits = f.m_numHits;
      for (int i=0; i < f.m_numHits; ++i)
        this->m_hits[i] = f.m_hits[i];
      this->m_quality = f.m_quality;
      this->m_ax     = f.m_ax;
      this->m_bx     = f.m_bx;
      this->m_cx     = f.m_cx;
      this->m_dx     = f.m_dx;
      this->m_ay     = f.m_ay;
      this->m_by     = f.m_by;
      this->m_cosAfter = f.m_cosAfter;
      this->m_chi2PerDoF = f.m_chi2PerDoF;
      this->m_nDoF   = f.m_nDoF;
      this->m_zMagnet = f.m_zMagnet;
 
      return *this;
    }

//  private:
 
    public: 
    __device__ void init( const VeloTrack tr ) {
      // velo tracks data
      m_vtid    = tr.id;
      m_x0      = tr.x0; 
      m_y0      = tr.y0;
      m_z0      = tr.z0;
      m_tx      = tr.tx;
      m_ty      = tr.ty;
      m_qoverp  = tr.qoverp;
      m_ignore  = true;
      m_numHits = 0;
      m_quality = 0.0;
      m_ax = 0.0;
      m_bx = 0.0;
      m_cx = 0.0;
      m_dx = 0.0;
      m_ay = 0.0;
      m_by = 0.0;
      m_cosAfter   = 1.0;
      m_zMagnet    = 0.0;
      m_chi2PerDoF = 0.0;
      m_nDoF       = 0;      
    }
    
//  public:

    __device__ ~FwdTrack( ) {}; ///< Destructor
    // getters
    __device__ double xStraight( double z ) const { return m_x0 + (z-m_z0) * m_tx; }
    __device__ double yStraight( double z ) const { return m_y0 + (z-m_z0) * m_ty; }

    __device__ double slX()                const { return m_tx;        }
    __device__ double slY()                const { return m_ty;        }
    __device__ double slX2()               const { return m_tx * m_tx; }
    __device__ double slY2()               const { return m_ty * m_ty; }      
    __device__ double qoverp()             const { return m_qoverp; }
    
    __device__ double sinTrack()           const { return sqrt( 1. - 1./(1. + m_tx*m_tx + m_ty*m_ty) ); }
    __device__ double pt()                 const { return fabs(m_qoverp) > 1e-20 ? sinTrack() / fabs(m_qoverp): 9999999.0; }

    __device__ int getNumHits()            const { return m_numHits; }
    __device__ FwdHit hit(int idx)               { return m_hits[idx]; }
    __device__ FwdHit* hitptr(int idx)           { return &m_hits[idx]; }
    __device__ FwdHit* hits()                    { return m_hits; }
    
    __device__ bool ignore()               const { return m_ignore; }
    
    __device__ void setChi2PerDoF( double chi2 ) { m_chi2PerDoF = chi2; }
    __device__ double chi2PerDoF()         const { return m_chi2PerDoF; }

    __device__ int nDoF()                  const { return m_nDoF; }
   
    __device__ double x( double dz )       const {  
      return m_ax + dz * ( m_bx + dz * ( m_cx + dz * m_dx ) ); }
      
    __device__ double y( double dz )       const { 
      return m_ay + dz * m_by; }
      
    __device__ double quality()            const { return m_quality; }   
    __device__ double xSlope( double dz )  const { 
      return m_bx + dz * ( 2 * m_cx + 3 * dz * m_dx ); }
      
    __device__ double ySlope( )            const { return m_by; }
    __device__ double dSlope()             const { return m_bx - m_tx; }
    
    __device__ double xMagnet( double dz ) const { return m_ax + dz * m_bx; }
    __device__ double zMagnet( )           const { return m_zMagnet; }

    __device__ double cosAfter()           const { return  m_cosAfter; }
    
    __device__ int getVeloTk()             const { return m_vtid; }
    __device__ double ax()                 const { return m_ax; }
    __device__ double bx()                 const { return m_bx;   }
    __device__ double cx()                 const { return m_cx; }
    __device__ double dx()                 const { return m_dx; }
    __device__ double ay()                 const { return m_ay; }

    __device__ double distAtMagnetCenter( ) const {
      return xStraight( m_zMagnet ) - xMagnet ( m_zMagnet - ZMidT );
    }  
 
    __device__ int exist( FwdHit fhit ) { 
      for (int i = 0; i < m_numHits; ++i) 
        if ( fhit.index() == m_hits[i].index() )
          return i;
      return -1;          
    }
    
    
    __device__ void setNDoF( int nDoF )     { m_nDoF = nDoF; }    
    __device__ void setQuality( double q )  { m_quality = q; }
    __device__ void setIgnore( bool v )     { m_ignore = v; }
    
    // TODO Do we need an atomic version?
    __device__ void addHit( FwdHit fhit ) { 
      if ( ( exist(fhit) == -1 ) && ( m_numHits < FWDHITS ) )
        m_hits[m_numHits++] = fhit; 
      else
        printf("addHit %d, error (%d)!\n", fhit.index(), m_numHits);    
    }

    // Remove unSelected hits
    __device__ void cleanHits( ) {
      int nValidHits = 0;
      for ( int i = 0; i < m_numHits; ++i) {
        if ( m_hits[i].isSelected( ) ) {
          if ( nValidHits < i )
            m_hits[nValidHits] = m_hits[i];
          nValidHits++;
        }
      }
      m_numHits = nValidHits;
    }
    
    __device__ void tagHits() {
      for ( int i = 0; i < m_numHits; ++i) {  
        if ( m_hits[i].isSelected() ) 
          m_hits[i].setIgnored( true ); 
      }
    }  
    

    __device__ void setParameters( double ax, double bx, double cx, double dx, double ay, double by, double zMagnet ) {
      m_ax = ax;
      m_bx = bx;
      m_cx = cx;
      m_dx = dx;
      m_ay = ay;
      m_by = by;
      m_cosAfter =  1. / sqrt( 1. +  m_bx * m_bx  );
      //m_fitted = true;
      m_zMagnet = zMagnet;
    }

    __device__ void updateParameters( double dax, double dbx, double dcx,
                           double ddx=0., double day=0., double dby= 0.  ) {                  
      m_ax += dax;
      m_bx += dbx;
      m_cx += dcx;
      m_dx += ddx;
      m_ay += day;
      m_by += dby;
      if ( dbx ) m_cosAfter =  1. / sqrt( 1. +  m_bx * m_bx  );
      
    }

    __device__ int countSelected() {
      int nbSelected = 0;
      for ( int i = 0; i < m_numHits; ++i)  
        if ( m_hits[i].isSelected() )  nbSelected++;
      return nbSelected;
    }

    __device__ int countIgnored() {
      int nbSelected = 0;
      for ( int i = 0; i < m_numHits; ++i)  
        if ( m_hits[i].isIgnored() ) nbSelected++;
      return nbSelected;
    }  

    __device__ int setSelectedHits( ) {
      int nbSelected = 0;
      for ( int i = 0; i < m_numHits; ++i) {
        if ( m_hits[i].isIgnored() ) {
          m_hits[i].setSelected( false );
        } else  {
          m_hits[i].setSelected( true );
          nbSelected++;
        }
      }
      return nbSelected;
    }

    __device__ void sortbyZ() {
      int idx;

      for (int i = 0; i < m_numHits-1; ++i) {
        idx = i;
        for (int j = i + 1; j < m_numHits; ++j)
          if ( m_hits[j].z() < m_hits[idx].z() ) idx = j;
        if ( i != idx ) {
          FwdHit tmp = m_hits[idx];
          m_hits[idx] = m_hits[i];
          m_hits[i] = tmp;
        }   
      }
    }

    __device__ void debug(int event, int vt) {
      printf("[%d, %d] DEBUG FwdTrack Chi2/nDof = %f nDof %d X cord size = %d\n", 
            event, vt, m_chi2PerDoF, m_nDoF, m_numHits );
      printf("[%d, %d] DEBUG FwdTrack Parameters: %f %f %f %f and %f %f\n", 
            event, vt, m_ax, m_bx, m_cx, m_dx, m_ay, m_by );      
          
      for (int i = 0; i < m_numHits; ++i)
        m_hits[i].debug(event, vt);      
      
    }


  protected:

  private:
    int    m_vtid;
    double m_x0;
    double m_y0;
    double m_z0;
    double m_tx;
    double m_ty;
    double m_qoverp;
    // it is always 0 in Velo context
    //double m_qOverP;
    bool   m_ignore;
    // hits in the track
    unsigned int m_numHits;
    FwdHit m_hits[FWDHITS];
    double m_quality;
    // fit parameters
    double m_ax;
    double m_bx;
    double m_cx;
    double m_dx;
    double m_ay;
    double m_by;
    double m_cosAfter;
    double m_zMagnet;
    double m_chi2PerDoF;
    int    m_nDoF;
    
    
  };
  
typedef PVector<FwdTrack, MAXTRACKS> CandidateTk;
typedef PVector<CandidateTk, VELOTK> FwdTracks;
typedef PVector<FwdHit, MaxHitXRef> FwdHits;
//=============================================================================
// Planes and regions Counter class
//=============================================================================

class FwdCounter {
  public:

    /// Standard constructor
    
    __device__ FwdCounter(FwdHit* fhits, int start, int end ) : m_nbDifferent(0) {
      for (int i = 0; i < PLANES; ++i) 
        m_planeList[i] = 0;
      for (int i = 0; i < REGIONS; ++i) 
        m_regionList[i] = 0;  
        
      for (int i = start; i < end; ++i) {
        if ( fhits[i].isSelected() ) { 
            if ( 0 == m_planeList[ fhits[i].planeCode() ]++ ) ++m_nbDifferent;
            ++m_regionList[ fhits[i].region() ];
        }
      }
    }

    /// add a hit to be counted, returns # of different planes
    __device__ int addHit( const FwdHit& fhit ) {
      if ( !fhit.isSelected() ) return m_nbDifferent;
      ++m_regionList[ fhit.region() ];
      if ( 0 == m_planeList[ fhit.planeCode() ]++ ) ++m_nbDifferent;
      return m_nbDifferent;
    }
    
     __device__ int removeHit( const FwdHit& fhit ) {
      if ( !fhit.isSelected() ) return m_nbDifferent;
      --m_regionList[ fhit.region() ];
      if ( 0 == --m_planeList[ fhit.planeCode() ] ) --m_nbDifferent;
      return m_nbDifferent;
    } 


    /// returns number of different planes
    __device__ int nbDifferent() const { return m_nbDifferent; }

    /// returns number of hits in specified plane
    __device__ int nbInPlane( int plane ) const { return m_planeList[plane]; }

    /// return number of hits in specified region
    __device__ int nbInRegion( int region ) const
    { return m_regionList[region]; }

    /// return number of hits counted in IT regions [2..5]
    __device__ int nbIT() const {
      return m_regionList[2] + m_regionList[3] + m_regionList[4] + m_regionList[5];  
    }

    /// return number of hits counted in OT regions (0 and 1)
    __device__ int nbOT() const {
      return m_regionList[0] + m_regionList[1];
    }

    /// returns number of stereo planes with hits
    __device__ int nbStereo() const {
      int nb = 0;
      if ( m_planeList[ 1] !=0 ) ++nb;
      if ( m_planeList[ 2] !=0 ) ++nb;
      if ( m_planeList[ 5] !=0 ) ++nb;
      if ( m_planeList[ 6] !=0 ) ++nb;
      if ( m_planeList[ 9] !=0 ) ++nb;
      if ( m_planeList[10] !=0 ) ++nb;
      return nb;
    }

    /// returns the number of plane with most hits
    /// -1 if there aren't any hits.
    __device__ int maxPlane () const
    {
      int max = 0;
      int imax = -1;
      for (int i = 0; i < PLANES; ++i ) {
        if ( m_planeList[i] > max ) { 
          max = m_planeList[i];
          imax = i;
        }
      }    
      return imax;
    }

    /// return the region with most hits
    /// -1 if there aren't any hits.
    __device__ int maxRegion() const
    {
      int max = 0;
      int imax = -1;
      for (int i = 0; i < REGIONS; ++i ) {
        if ( m_regionList[i] > max ) { 
          max = m_regionList[i];
          imax = i;
        }
      }    
      return imax;
    }
    
  private:

    int m_regionList[REGIONS];
    int m_planeList[PLANES];
    int m_nbDifferent;

};



//// Velo tracks



class VeloTracks {
  public:
    // constructor
    __device__ VeloTracks(): m_notracks(0) {}
    
    // destructor
    __device__ ~VeloTracks() {}
  
    __device__ void setSize( int size ) { 
      if ( size > VELOTK )
        printf("ERROR! Too much Velo tracks, we consider only %d tracks\n", VELOTK);
      m_notracks = min(size, VELOTK); }

    __device__ void addTrack(int idx, int id, float x, float y, float z, float tx, float ty, float qoverp) {
      if ( idx >= m_notracks ) {
        printf("ERROR! %d is not valid velotrack index!\n", idx);
        return;
      }
      m_id[idx] = id;    
      m_x0[idx] = x;
      m_y0[idx] = y;
      m_z0[idx] = z;
      m_tx[idx] = tx;
      m_ty[idx] = ty;
      m_qoverp[idx] = qoverp;
    }  
    

    __device__ void addTrack(int id, float x, float y, float z, float tx, float ty, float qoverp) {
      if ( m_notracks >= VELOTK ) {
        printf("ERROR! Too many tracks!\n");
        return;
      }
      addTrack(m_notracks, id, x, y, z, tx, ty, qoverp);
      m_notracks++;
    }
  
    __device__ int size() {
      return m_notracks;
    }
    
    __device__ void debug(int idx) {
      printf("*** DEBUG VeloTrack %d/%d id %d x %f y %f tx %f ty %f z %f q/p %f\n", idx, m_notracks,
              m_id[idx], m_x0[idx], m_y0[idx], m_tx[idx], m_ty[idx], m_z0[idx], m_qoverp[idx]);
      float x = m_x0[idx] + (1640-m_z0[idx]) * m_tx[idx];
      float y = m_y0[idx] + (1640-m_z0[idx]) * m_ty[idx];
      printf("*** DEBUG VeloTrack %d/%d at zAfetrVelo=1640mm x %f y %f\n", idx, m_notracks, x, y);       
    }
      
  
    __device__ VeloTrack getTrack(int idx) const {
      VeloTrack vt;
      vt.id = m_id[idx];
      vt.x0 = m_x0[idx];
      vt.y0 = m_y0[idx];
      vt.z0 = m_z0[idx];
      vt.tx = m_tx[idx];
      vt.ty = m_ty[idx];
      vt.qoverp = m_qoverp[idx];
      return vt;
    }

  private:
    int m_notracks;

    int   m_id[VELOTK];
    float m_x0[VELOTK];
    float m_tx[VELOTK];
    float m_y0[VELOTK];
    float m_ty[VELOTK];
    float m_z0[VELOTK];
    float m_qoverp[VELOTK];
};



#endif 

