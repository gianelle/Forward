#ifndef FWDKERNELS_CU
#define FWDKERNELS_CU

#include "FwdKernels.cuh"
#include <math.h>
#include "FwdTool.cuh"
#include "FwdData.cuh"
#include "FwdDefinitions.cuh"

__device__ __constant__ double c_zMagnetParams[5];
__device__ __constant__ double c_xParams[2];
__device__ __constant__ double c_yParams[2];
__device__ __constant__ double c_momentumParams[6];

__device__ void sortByProjection(FwdHit* list, const int size) {
  
  int idx;
  
  for (int i = 0; i < size-1; ++i) {
    idx = i;
    for (int j = i + 1; j < size; ++j)
      if ( list[j].projection( ) < list[idx].projection( ) ) idx = j;
    if ( i != idx ) {
      FwdHit tmp = list[idx];
      list[idx] = list[i];
      list[i] = tmp;
    }
  }
  
}


__device__ void sortByProjection(FwdHit** list, const int size) {
  
  int idx;

  for (int i = 0; i < size-1; ++i) {
    idx = i;
    for (int j = i + 1; j < size; ++j)
      if ( list[j]->projection( ) < list[idx]->projection( ) ) idx = j;
    if ( i != idx ) {
      FwdHit* tmp = list[idx];
      list[idx] = list[i];
      list[i] = tmp;
    }   
  }
}


__device__ bool countPlanes(FwdHits fhits, int minlim) {
  int planeList[PLANES];
  for (int i = 0; i < PLANES; ++i) 
    planeList[i] = 0;
  
  int planes = 0;
    
  for (int i = 0; i < fhits.size(); ++i) {
    if ( fhits[i].isSelected() ) 
      if ( 0 == planeList[ fhits[i].planeCode() ]++ ) 
        ++planes;
    if ( planes >= minlim ) return true;
  }
  
  return false;       

}

//=========================================================================
//  Returns center of magnet for velo track
//=========================================================================
__device__ double zMagnet( const FwdTrack& track ) {
  //== correction behind magnet neglected
  const double zMagnet    = ( c_zMagnetParams[0] +
			  c_zMagnetParams[2] * track.slX2() +
			  c_zMagnetParams[4] * track.slY2() );
  return zMagnet;
}

//=========================================================================
// define the XInterval see fillXList
//=========================================================================
class XInterval {
  double m_zMagnet, m_xMagnet, m_txMin, m_txMax, m_xmin, m_xmax;
  public:
    __device__ XInterval(double zMagnet, double xMagnet, double txMin, double txMax, double xMin, double xMax):
       m_zMagnet(zMagnet), m_xMagnet(xMagnet), m_txMin(txMin), m_txMax(txMax), m_xmin(xMin), m_xmax(xMax) {}
    __device__ double xMinAtZ(double z) const { return m_txMin*(z-m_zMagnet)+m_xMagnet; }
    __device__ double xMaxAtZ(double z) const { return m_txMax*(z-m_zMagnet)+m_xMagnet; }
    //__device__ double xKick(double z) const { return m_xscale * z - m_offset; }
    __device__ double xMin() const { return m_xmin; } 
    __device__ double xMax() const { return m_xmax; }
    __device__ bool inside(double x) const { return m_xmin <= x && x < m_xmax; }
    __device__ bool outside(double x) const { return x < m_xmin || m_xmax <= x ; }
};

__device__ double dSlope_kick( double pt, double sinTrack ) {
    return sinTrack * c_magnetKickParams0 * 1000 / ( pt - sinTrack * c_magnetKickParams1 * 1000);
}

__device__ XInterval make_XInterval( const FwdTrack& track ) {
  double xExtrap = track.xStraight( ZMidT );
  //== calculate center of magnet from Velo track
  const double zmagnet =  zMagnet( track );
  //== calculate if minPt or minMomentum sets the window size
  const double dSlope = dSlope_kick( 1000*c_minPt, track.sinTrack() );
  const double dz     = ZMidT - zmagnet;
  const double maxRange = dSlope*dz;
  double xMin = xExtrap - maxRange;
  double xMax = xExtrap + maxRange;
  double dSlopeMin = -dSlope;
  double dSlopeMax =  dSlope;
  
  //== based on momentum a wrong-charge sign window size is defined
  //if (m_useMomentumEstimate && !m_withoutBField && track.qOverP() != 0 ) 
  // useMomentumEstimate = True in HLT1; = False in HLT2
  
  if ( track.qoverp() != 0 ) {
    //bool useKick { m_UseWrongSignWindow && track.track()->pt() > m_WrongSignPT };
    // UseWrongSignWindow = True in HLT1; = False in HLT2
    bool useKick = ( track.pt() > c_WrongSignPT );
    double kickRange = useKick ? dSlope_kick( c_WrongSignPT, track.sinTrack() )*dz : 0;
   //if ( std::signbit( track.qOverP() ) != std::signbit( m_fwdTool->magscalefactor() ) ) 
    if ( track.qoverp() < 0 ) {
      xMin = xExtrap - kickRange;
      dSlopeMin = -kickRange/dz;
    } else {
      xMax = xExtrap + kickRange;
      dSlopeMax = kickRange/dz; 
    }
  }
  
   //printf("ALE DBG *** xExtrap %f q/p %f pt %f sin %f dz %f dslope %f range (%f %f)\n", xExtrap, track.qoverp(), track.pt(), track.sinTrack(), dz,  dSlope_kick( 1000*c_minPt, track.sinTrack() ), xMin, xMax);
    
  // m_useProperMomentumEstimate = False
    

  return XInterval(zmagnet, track.xStraight( zmagnet ),
          track.slX()+dSlopeMin,
          track.slX()+dSlopeMax,
          xMin, xMax);
};



//=============================================================================
__device__ void updateHitForTrack ( FwdHitsEvContainer* fwdHits, FwdHit* fhit, 
                                  const double y0, const double dyDz) {

  const double zy0 = fwdHits->zAtYEq0( fhit->index() );
  const double y  = ( y0 + dyDz * zy0 ) / ( 1. - fhit->dzDy( ) * dyDz );

  // ????? l'e' un burdel! TODO TODO
  /*
  if (fwdHits->isOT( idx ))
    hit.setDriftDistance(otHit->untruncatedDriftDistance(y));
  */
 
  fhit->setZ( zy0 + y * fhit->dzDy( ) );
  fhit->setX( fwdHits->xAtYEq0( fhit->index() ) + y * fhit->dxDy( ) ); 

}
  
//=============================================================================
__device__ void updateHitsForTrack ( FwdHitsEvContainer* fwdHits, FwdTrack* track,
                                     int itBeg, int itEnd )  {
  for ( int it = itBeg; it < itEnd ; ++it ) {
    FwdHit* fhit = track->hitptr(it);
    double sly = track->ySlope();
    double y0  = track->y( fhit->z( ) - ZMidT ) - fhit->z( ) * sly;
    updateHitForTrack( fwdHits, fhit, y0, sly);
  }
}

//=============================================================================

__device__ double distanceHitToTrack( const FwdTrack& track, const FwdHit hit)  {    
                       
  double dist = hit.x() - track.x( hit.z() - ZMidT );
  if ( !hit.isOT() ) return dist; // IT hit
  
  // OT : distance to a circle, drift time
  dist = dist * track.cosAfter();
  double dx = hit.driftDistance();
  
  if ( 0 !=  hit.rlAmb() ) return dist + hit.rlAmb() * dx;
  // Take closest distance in the rest
  if ( fabs( dist - dx ) < fabs( dist + dx ) ) {
    return dist - dx;
  }
  return dist + dx;
}


//=============================================================================

__device__ double distanceForFit( const FwdTrack& track, const FwdHit hit)  {
  double dist =  distanceHitToTrack( track, hit );
  if ( !hit.isOT( ) )  return dist;
  return dist / track.cosAfter();
}

//=============================================================================

__device__ double chi2Hit( const FwdTrack& track, 
                           const float weight, const FwdHit hit )  {
  double dist = distanceHitToTrack( track, hit );
  return dist * dist * weight;
}


//=========================================================================
//  Set the RL flag for OT s, if obvious.
//=========================================================================

__device__ void setRlDefault( FwdTrack* track, int itBeg, int itEnd )  {
  int itH;
  PVector <FwdHit*, FWDHITS> temp;
  for ( int planeCode = 0; planeCode < PLANES; ++planeCode ) {
    temp.empty();
    for ( itH = itBeg; itH < itEnd; ++itH ) {
      FwdHit* hit = track->hitptr(itH);
      if ( planeCode != hit->planeCode( ) ) continue;
      hit->setRlAmb( 0 );   // default
      if ( !hit->isOT( ) ) continue;    // IT ->no ambiguity!
      temp.add( hit );
    }
    if ( 2 > temp.size() ) continue;   // no RL solved if at most one hit

    sortByProjection(temp.elements(), temp.size());

    double prevDistM = 10.;
    double prevDistP = 10.;
    FwdHit* prevHit = temp[0];

    for ( itH = 0; itH < temp.size(); ++itH ) {
      FwdHit* hit = temp[itH];
      hit->setRlAmb( -1 );
      double distM = distanceHitToTrack( *track, *hit );
      hit->setRlAmb( +1 );
      double distP = distanceHitToTrack( *track, *hit );
      hit->setRlAmb( 0 );

      double minDist = 0.3;
      short  vP = 0;
      short  vC = 0;

      if ( fabs( distM - prevDistP ) < minDist ) {
        minDist =  fabs( distM - prevDistP );  vP = +1;  vC = -1;
      }
      if ( fabs( distP - prevDistM ) < minDist ) {
        minDist =  fabs( distP - prevDistM );  vP = -1;  vC = +1;
      }
      prevHit->setRlAmb( vP );
      hit->setRlAmb( vC );

      prevHit   = hit;
      prevDistP = distP;
      prevDistM = distM;

    }
  }
}

//=========================================================================
//  Compute the chi2 per DoF of the track
//=========================================================================

__device__ double chi2PerDoF ( FwdHitsEvContainer* fwdHits, FwdTrack* track )  {
  double totChi2 = 0.;
  int nDof = 0;
  bool hasStereo = false;

  //== Error component due to the contraint of the magnet centre
  double dist      = track->distAtMagnetCenter( );
  double errCenter = c_xMagnetTol + track->dSlope() * track->dSlope() * c_xMagnetTolSlope;
  totChi2 = dist * dist / errCenter;

  int itH;
  for ( itH = 0; itH < track->getNumHits(); ++itH ) {
    FwdHit hit = track->hit(itH);
    if ( !hit.isSelected( ) ) continue;
    double chi2 = chi2Hit( *track, fwdHits->weight( hit.index() ), hit );
    totChi2 += chi2;
    nDof    += 1;
    if ( !(hit.layer( ) == 0 || hit.layer( ) == 3) ) hasStereo = true;
  }
  nDof -= 2;  // Fitted parabola in X, constraint to magnet centre
  if ( hasStereo ) nDof -= 1;  // Linear fit, magnet centre constraint

  totChi2 /= nDof;

  track->setChi2PerDoF( totChi2 );
  track->setNDoF( nDof );

  return totChi2;
}

//=========================================================================
//  Returns the q/p of the track
//=========================================================================
__device__ double qOverP ( const FwdTrack& track )  {
  // double qop(1.0/Gaudi::Units::GeV) ;
  double qop = 1.0; 
  double magscalefactor = c_signedRelativeCurrent;
  if( std::abs(magscalefactor) > 1e-6 ) {
    double bx = track.bx();
    double bx2 = bx * bx;
    double coef = ( c_momentumParams[0] +
		    c_momentumParams[1] * bx2 +
		    c_momentumParams[2] * bx2 * bx2 +
		    c_momentumParams[3] * bx * track.slX() +
		    c_momentumParams[4] * track.slY2() +
		    c_momentumParams[5] * track.slY2() * track.slY2() );
    double proj = sqrt( ( 1. + track.slX2() + track.slY2() ) / ( 1. + track.slX2() ) );
    qop = track.dSlope() / ( coef * proj * magscalefactor * (-1)) ;
  }
  return qop/1000;
}


//=============================================================================
// return the value of coordinate x at the reference plane (z = ZMidT = 8520mm)
//=============================================================================
__device__ double xAtReferencePlane( FwdTrack& track, const FwdHit hit, bool store ) {

  double x;

//  if (!m_withoutBField){

    double zHit       = hit.z();
    double xHit       = hit.x();  

    double zMagnet    = c_zMagnetParams[0] +
			  c_zMagnetParams[2] * track.slX2() +
			  c_zMagnetParams[3] * xHit * xHit +
			  c_zMagnetParams[4] * track.slY2();
    double xMagnet    = track.xStraight( zMagnet );

    if ( hit.isOT() ) {
      if ( hit.hasNext()     ) xHit += hit.driftDistance();
      if ( hit.hasPrevious() ) xHit -= hit.driftDistance();
    }
    
    double slopeAfter = ( xHit - xMagnet ) / ( zHit - zMagnet );
    double dSlope     = slopeAfter - track.slX();
    double dSl2       = dSlope * dSlope;
    zMagnet           = zMagnet + c_zMagnetParams[1] * dSl2;
    double dz         = 1.e-3 * ( zHit - ZMidT );
    double dyCoef     = dSl2 * track.slY();
    double dy         = dyCoef * ( c_yParams[0] + dz * c_yParams[1] );
    double dxCoef     = dz * dz * ( c_xParams[0] + c_xParams[1] * dz );
    xHit              = xHit + dy * hit.dxDy( ) - dxCoef * dSlope ;
    xMagnet           = track.xStraight( zMagnet );
    slopeAfter        = ( xHit - xMagnet ) / ( zHit - zMagnet );
    x                 = xMagnet + ( ZMidT - zMagnet ) * slopeAfter;

    if ( store ) {

      track.setParameters( x,
			   slopeAfter,
			   1.e-6 * c_xParams[0] * dSlope,
			   1.e-9 * c_xParams[1] * dSlope,
			   track.yStraight( ZMidT ) + dyCoef * c_yParams[0],
			   track.slY() + dyCoef * c_yParams[1], zMagnet );    
      
	  }		    
  /*
  // without B field
  } else {
     
    double zMagnet    = 0.0;
    double xMagnet    = track.xStraight( zMagnet);
    double zHit       = hit->z();
    double xHit       = hit->x();
     
    const Tf::OTHit* othit = hit->hit()->othit();
    
    if ( othit ) {
      if ( hit->hasNext()     ) xHit += hit->driftDistance();
      if ( hit->hasPrevious() ) xHit -= hit->driftDistance();
    }
    
    double slopeAfter = ( xHit - xMagnet ) / ( zHit - zMagnet );
    
    x                 = xMagnet + ( m_zReference - zMagnet ) * slopeAfter;
    if ( store ) {
      track.setParameters( x, 
			   ( x - xMagnet ) / ( m_zReference - zMagnet ),
			   0, 
			   0, 
			   track.yStraight( m_zReference ),
			   track.slY());
      m_zMagnet = zMagnet;
    }
  }
  */
  return x;
}


__device__ double changeInY( const FwdTrack& track )  {
  double yOriginal = track.yStraight( ZMidT );
  yOriginal += track.dSlope() * track.dSlope() * track.slY() * c_yParams[0];
  return yOriginal - track.y( 0. );
}

__device__ double computeQuality( const FwdTrack& track, double qOverP )  {
  double quality  = 5. * fabs(  changeInY( track ) ) / ( c_maxDeltaY + qOverP * qOverP * c_maxDeltaYSlope );
  quality += track.chi2PerDoF() / 10.;
  quality += 10 * fabs(qOverP);  // low momentum are worse
  return quality;
}

__device__ bool inCenter( const FwdTrack& track )  {
  return  c_centerOTYSize > fabs( track.y( 0. ) );
}

__device__ bool passMomentum( const FwdTrack& c, double sinTrack )  {
  const double momentum = 1.0 / fabs(qOverP( c ));
  const double pt = sinTrack*momentum;
  //== reject if below threshold
  return  ( momentum > c_minMomentum && pt > c_minPt) ;
}

//=========================================================================
//  Parabolic fit to projection.
//=========================================================================

__device__ bool fitXProjection ( FwdHitsEvContainer* fwdHits, FwdTrack* track,
                                  int itBeg, int itEnd,
                                  bool onlyXPlanes  )  {

  double errCenter = c_xMagnetTol + track->dSlope() * track->dSlope() * c_xMagnetTolSlope;
  for ( unsigned int kk = 0 ; 10 > kk ; ++kk ) {
    //= Fit the straight line, forcing the magnet centre. Use only position and slope.

    double dz   = track->zMagnet() - ZMidT;
    double dist = track->distAtMagnetCenter( );
    double w    = 1./errCenter;

    FitParabola  parabola(dz, dist, w);
    /*
    FwdFitLine      line;
    if (m_withoutBField){
      line.addPoint(dz,dist,w);
    }
    */

    for (int itH = itBeg; itH < itEnd; ++itH) {  
      FwdHit hit = track->hit(itH);
      int idx = hit.index();
      if ( !hit.isSelected() ) continue;
      if ( onlyXPlanes && !(hit.layer() == 0 || hit.layer() == 3) ) continue;
      double dist2 = distanceForFit( *track, hit );
      dz    = hit.z() - ZMidT;
      w     = fwdHits->weight( idx ); 
//      if (!m_withoutBField)
      parabola.addPoint( dz, dist2, w );
//      else {
//        line.addPoint(dz, dist2, w);
//     }
    }

    double dax, dbx, dcx;
    
//    if (!m_withoutBField){
    if (!parabola.solve()) return false;
    dax = parabola.ax();
    dbx = parabola.bx();
    dcx = parabola.cx();   
/*    } else {
      if (!line.solve()) return false;
      dax = line.ax();
      dbx = line.bx();
      dcx = 0.0;    
    }
  */  
    
    track->updateParameters( dax, dbx, dcx );

    if ( fabs( dax ) < 5.e-3 &&
         fabs( dbx ) < 5.e-6 &&
         fabs( dcx ) < 5.e-9    ) break;  // wait until stable, due to OT.
  }
  return true;
}





//=========================================================================
//  Fit the X projection of a FwdTrack. Iterative?.
//=========================================================================

__device__ bool fitXCandidate ( FwdHitsEvContainer* fwdHits, FwdTrack* track, 
                                const double maxChi2, const int minPlanes ) {
  if ( minPlanes > track->setSelectedHits( ) ) return false;
  FwdCounter maxPlanes( track->hits(), 0, track->getNumHits() );
  int bestPlanes = maxPlanes.nbDifferent();
  if ( minPlanes > bestPlanes ) return false;

  //=== Is there a region with 6 planes ?
  // Is there anybody out there?

  int itH;
  int bestRegion = -1;
  double spread = 1000.;

  for( unsigned int maxRegion = 0; maxRegion < REGIONS; ++maxRegion ) {
    if ( maxPlanes.nbInRegion( maxRegion ) >= 6 ) {  // count by plane
      FwdCounter planes( track->hits(), 0, track->getNumHits() );
      double first = 1.e7;
      double last  = first;
      for ( itH = 0; itH < track->getNumHits(); ++itH ) {
        FwdHit fhit = track->hit(itH);
        unsigned int region = fhit.region( );
	      if ( region == maxRegion ) {
          if ( first > 1.e6 ) first = fhit.projection();
          last = fhit.projection();
          planes.addHit( fhit );
        }
      }
      if ( planes.nbDifferent() == 6 ) {
        double mySpread = last-first;
        if ( mySpread < spread ) {
          spread = mySpread;
          bestRegion = maxRegion;
        }
      }
    }
  }
  
  if ( 0 <= bestRegion ) {
    // remove other regions !
    for ( itH = 0; itH < track->getNumHits(); ++itH ) {
      FwdHit* hit = track->hitptr(itH);
      if ( hit->region( ) != bestRegion ) {
        hit->setSelected( false );
      }
    }
  }

  int itBeg, itEnd;
  itBeg = 0;
  itEnd = itBeg + bestPlanes;

  //== get enough planes fired
  FwdCounter planeCount1( track->hits(), itBeg, itEnd);
  while ( itEnd < track->getNumHits() && bestPlanes > planeCount1.nbDifferent() ) {
    FwdHit* hit = track->hitptr(itEnd++);
    if ( hit->isSelected( ) ) planeCount1.addHit( *hit );
  }
  if ( bestPlanes > planeCount1.nbDifferent() ) return false;
  double minDist = track->hit(itEnd-1).projection() - track->hit(itBeg).projection();

  int itBest = itBeg;
  int itLast = itEnd;

  //== Better range ? Remove first, try to complete, measure spread...
  while ( itEnd < track->getNumHits() &&  bestPlanes <= planeCount1.nbDifferent() ) {
    planeCount1.removeHit( track->hit(itBeg++) );
    while ( itEnd < track->getNumHits() && bestPlanes > planeCount1.nbDifferent() ) {
      FwdHit* hit = track->hitptr(itEnd++);
      if ( hit->isSelected( ) ) planeCount1.addHit( *hit );
    }
    if ( bestPlanes <= planeCount1.nbDifferent() ) {
      if ( minDist > track->hit(itEnd-1).projection() - track->hit(itBeg).projection() ) {
        minDist = track->hit(itEnd-1).projection() - track->hit(itBeg).projection();
        itBest = itBeg;
        itLast = itEnd;
        if ( bestPlanes < planeCount1.nbDifferent() )  bestPlanes = planeCount1.nbDifferent();
      }
    }
  }

  //== OK, itBest is the start point...
  itBeg = itBest;
  itEnd = itLast;

  //== Add hits before/after
  FwdCounter regions( track->hits(), itBeg, itEnd );
  double tolSide = .2;
  if ( 2 > regions.maxRegion() ) tolSide = 2.0;

  double minProj = track->hit(itBeg).projection() - tolSide;
  while ( itBeg > 0 ) {
    FwdHit* hit = track->hitptr(--itBeg); 
    if ( minProj > hit->projection() ) {
      ++itBeg;
      break;
    }
  }
  double maxProj = track->hit(itEnd-1).projection() + tolSide;
  while ( itEnd < track->getNumHits() ) {
    FwdHit* hit = track->hitptr(itEnd);
    if ( maxProj < hit->projection() )  break;
    ++itEnd;
  }

  FwdCounter planeCount( track->hits(), itBeg, itEnd );
  
  // initial value;
  int minHit = (itEnd - itBeg) / 2;
  
  xAtReferencePlane( *track, track->hit(minHit), true );
  updateHitsForTrack( fwdHits, track, itBeg, itEnd );
  setRlDefault( track, itBeg, itEnd );
  bool first = true;
  double highestChi2 = 1.e10;

  while ( maxChi2 < highestChi2 && minPlanes <= planeCount.nbDifferent() ) {

    if (!fitXProjection( fwdHits, track, itBeg, itEnd, true )) {
      printf("Error: Matrix not positive definite. [%d,%d]\n", itBeg, itEnd);
      return false;
    }
    
    highestChi2 = 0;
    FwdHit* worst = track->hitptr(itBeg);

    for ( itH = itBeg; itH < itEnd; ++itH ) {
      FwdHit* hit = track->hitptr(itH);
      if ( !hit->isSelected( ) ) continue;
      double chi2 = chi2Hit( *track, fwdHits->weight( hit->index() ), *hit );
      if ( highestChi2 < chi2 ) {
        highestChi2 = chi2;
        worst = track->hitptr(itH);
      }
    }

    if ( maxChi2 < highestChi2 ) {
      planeCount.removeHit( *worst );
      worst->setSelected( false );
    }
    
    if ( first && highestChi2 <  20 * maxChi2 ) {  // Add possibly removed hits
      first = false;
      bool hasNewHits = false;
      double minChi2 = maxChi2;
      if ( highestChi2 > maxChi2 ) {
        minChi2 = highestChi2 - 0.0001;  // down't find again the worst...
      }

      int oldItBeg = itBeg;
      for (itH = 0; itH < track->getNumHits(); ++itH) {
        FwdHit* hit = track->hitptr(itH);
        if ( hit->isIgnored() ) {
          hit->setSelected( false );
          continue;
        }

        bool close = minChi2 > chi2Hit( *track, fwdHits->weight( hit->index() ), *hit );
        if ( close ) {
          bool isNew = ( itH < oldItBeg || itH+1 > itEnd || !hit->isSelected() );
          if ( itH   < itBeg ) itBeg = itH;
          if ( itH+1 > itEnd ) itEnd = itH+1;
          if ( isNew ) hasNewHits = true;
        }
        hit->setSelected( close );

      }
      if ( hasNewHits ) {
        FwdCounter temp( track->hits(), itBeg, itEnd );
        planeCount = temp;  // update counter...
        highestChi2 = 2*maxChi2; // force an iteration...
      } 
    }
  }

  return minPlanes <= planeCount.nbDifferent();

}


__device__ bool fillStereoList ( FwdHitsEvContainer* hits, FwdTrack* track, double tol ) {

  PVector<FwdHit, 1200> temp;

  double sinT, sign, minProj;

  for (unsigned int sta = 0; sta < STATIONS; ++sta) {
    for (unsigned int lay = 1; lay < LAYERS-1; ++lay) { // u,v layers only
      if ( lay == 1 ) {
        sinT = -c_sinT;
        sign = -1.;
        minProj = tol +1.5;
      } else {
        sinT = c_sinT;
        sign = +1.;
        minProj = tol;
      }
      for (unsigned int region = 0; region < REGIONS; ++region) {

        double dz = hits->zRegion(sta, lay, region) - ZMidT;
        double yRegion = track->y( dz );

        if ( !hits->isYCompatible(sta, lay, region, yRegion, c_yCompatibleTol) ) continue;

        double xPred = track->x( dz );
        double xHitMin = xPred - fabs( yRegion * sinT ) - 40. - tol;

        //== get position and slope at z=0 from track at zReference (0 for y/ySlope functions)
        double ty = track->ySlope( );
        double y0 = track->y( 0. ) - ZMidT * ty;  // Extrapolate from back...

        for ( int i = 0; i < hits->numHits(sta, lay, region); ++i ) {
          FwdHit hit = hits->getFwdHit(sta, lay, region, i);
          if ( hit.x() <  xHitMin ) continue;

          //if ( hit.isIgnored() ) continue; // not safe
          if ( !hits->isYCompatible( hit.index(), yRegion, c_yCompatibleTol) ) continue;

          updateHitForTrack( hits, &hit, y0, ty );

          hit.setIgnored( false );
          hit.setSelected( true );
          if ( hit.isOT() &&
            ( c_minOTDrift > hit.driftDistance() || hit.driftDistance() > c_maxOTDrift ) ) {
            hit.setSelected( false );
            continue;
          }

          double xRef = ( hit.x() - xPred ) * sign;
          hit.setProjection( xRef );

          if ( -minProj > xRef * sign ) continue;
          if (  minProj < xRef * sign ) continue;  // or break?

          temp.add(hit);

        }
      }
    }
  }

      
  //== Sort by projection
  sortByProjection(temp.elements(), temp.size());
  PVector<FwdHit, 40> bestList; 

  int minYPlanes = 4;
  double maxSpread = 3.;
  
  if ( minYPlanes > temp.size() ) return false;  
  
  //== Select a coherent group
  int nbDifferent = 0;
  double size = 1000.;

  int itP, itB, itE, itF;

  for ( int itP = 0; itP <= temp.size() - minYPlanes; ++itP ) {
    itE = itP + minYPlanes -1;
    double spread = maxSpread;
    if ( temp[itP].isOT() ) spread += 1.5;  // OT drift ambiguities...
    
    //== If not enough hits in the maximum spread, skip
    if (  spread < temp[itE].projection() - temp[itP].projection() ) {
      while( spread < temp[itE].projection() - temp[itP].projection() ) itP++;
      --itP; // as there will be a ++ in the loop !
      continue;
    }

    //== Add all hits inside the maximum spread. If not enough planes, restart
    while ( ( itE != temp.size() ) &&
            ( spread > temp[itE].projection() - temp[itP].projection() ) ) itE++;
    FwdCounter planeCount( temp.elements(), itP, itE );
    //== Enough different planes
    if ( minYPlanes > planeCount.nbDifferent() ) continue;
    
    //== Try to make a single zone, by removing the first and adding other as
    //   long as the spread and minXPlanes conditions are met.
    itB = itP;
    itF = itE;
    while ( itP <= itE - minYPlanes && itF < temp.size() ) {
      planeCount.removeHit( temp[itP] );
      ++itP;
      while ( itF < temp.size() &&
              spread > temp[itF].projection() - temp[itP].projection() ) {
        planeCount.addHit( temp[itF++] );
      }
      if ( minYPlanes <= planeCount.nbDifferent() ) itE = itF;
    }

    double x1 = temp[itB].projection();
    double x2 = temp[itE-1].projection();

    //== We have the first list. The best one ????
    FwdCounter cnt( temp.elements(), itB, itE );
    if ( cnt.nbDifferent() >= nbDifferent ) {
      if ( cnt.nbDifferent() > nbDifferent || x2-x1 < size ) {
        nbDifferent =  cnt.nbDifferent();
        size = x2-x1;
        bestList.clear();
        for ( itP = itB; itE != itP; ++itP ) {
          bestList.add( temp[itP] );
        }
        //break; /// Keep first one !
      }
    }
    itP = --itE;
  }

  if ( minYPlanes > bestList.size() ) return false;
  
  for ( itP = 0; bestList.size() != itP; ++itP ) 
    track->addHit( bestList[itP] );
  
  //== Sort by Z
  track->sortbyZ();


  return true;

}

__device__ bool fitStereoCandidate (  FwdHitsEvContainer* fwdHits, FwdTrack* track,
                                      double maxChi2, int minPlanes )  {
                              
                                                   
  //== get enough planes fired
  FwdCounter planeCount( track->hits(), 0, track->getNumHits() );

  if ( minPlanes > planeCount.nbDifferent() ) { 
    return false;
  }

  updateHitsForTrack( fwdHits, track, 0, track->getNumHits() );
  setRlDefault( track, 0, track->getNumHits() );
  
  double highestChi2 = 10*maxChi2;
  bool   ignoreX = true;

  while ( highestChi2 > maxChi2 ) {
    //== Improve X parameterisation

    if (!fitXProjection( fwdHits, track, 0, track->getNumHits(), false )) {
      printf("Error: Matrix not positive definite.\n");
      return false;
    }

    for ( unsigned int kk = 0; 10 > kk; ++kk ) {
      FitLine line;
      for (int i = 0; i < track->getNumHits(); ++i ) {
        FwdHit hit = track->hit(i);
        if ( !hit.isSelected() || hit.layer() == 0 || hit.layer() == 3 ) continue;
        double dist = - distanceForFit( *track, hit ) / hit.dxDy();
        double dz   = hit.z() - ZMidT;
        double w    = fwdHits->weight( hit.index() ); 
        line.addPoint( dz, dist, w );
      }
      if ( !line.solve() ) {
        printf("Error: FitLineLoop: matrix not positive definite.\n");
        return false;
      }
      double day = line.ax();
      double dby = line.bx();
      track->updateParameters( 0., 0., 0., 0., day, dby );
      updateHitsForTrack( fwdHits, track, 0, track->getNumHits() );
      if ( fabs( day ) < 0.05 && fabs( dby ) < 0.00005 ) break;
    }

    highestChi2 = 0;
    FwdHit* worst = track->hitptr(0);
    for (int i = 0; i < track->getNumHits(); ++i ) {
      FwdHit* hit = track->hitptr(i);
      int idx = hit->index();
      if ( !hit->isSelected() ) continue;
      if ( ignoreX && ( hit->layer() == 0 || hit->layer() == 3 ) ) continue;   
      double chi2 = chi2Hit( *track, fwdHits->weight( idx ), *hit );
      if ( highestChi2 < chi2 ) {
        highestChi2 = chi2;
        worst = hit;
      }
    }  
    if ( highestChi2 > maxChi2 ) {  
      planeCount.removeHit( *worst );
      worst->setSelected( false );      
       //== Remove in one go all hits with bad contribution...
      if ( 1000. < highestChi2 ) {   
        for (int i = 0; i < track->getNumHits(); ++i ) {
          FwdHit* hit = track->hitptr(i);
          int idx = hit->index();
          if ( !hit->isSelected() ) continue;
          if ( ignoreX && ( hit->layer() == 0 || hit->layer() == 3 ) ) continue;   
          double chi2 = chi2Hit( *track, fwdHits->weight( idx ), *hit );
          if ( 1000. < chi2 ) {
            planeCount.removeHit( *hit ); 
            hit->setSelected( false );
          }
        }
      }
      if ( minPlanes > planeCount.nbDifferent() ) { 
        return false;
      }
    }
    //== If almost there, force one iteration with X fitting, at least...
    if ( highestChi2 < 2 * maxChi2 && ignoreX) {
      ignoreX = false;
      highestChi2 = 2.* maxChi2;
    }
  }
  if ( minPlanes > planeCount.nbDifferent() ) {
    return false; 
  }
  return true;
}    

__device__ bool removeYIncompatible( FwdHitsEvContainer* fwdHits, FwdTrack* track,
                                      double tol, int minPlanes )  {
  bool hasChanged = false;
  for ( int itH = 0; itH < track->getNumHits(); ++itH ) {
    FwdHit* hit = track->hitptr(itH);
    if ( !hit->isSelected() ) continue;
    if ( !fwdHits->isYCompatible(hit->index(), track->y( hit->z() - ZMidT), tol ) ) {
      hit->setSelected( false );
      hasChanged = true;
    }
  }        
  if ( hasChanged ) return fitStereoCandidate( fwdHits, track, 1000000., minPlanes );
  FwdCounter planeCount( track->hits(), 0, track->getNumHits());

  return planeCount.nbDifferent() >= minPlanes;
}








void setDetectorParameters() {
  cudaMemcpyToSymbol(c_zMagnetParams, &m_zMagnetParams, 5*sizeof(double));
  cudaMemcpyToSymbol(c_xParams, &m_xParams, 2*sizeof(double));
  cudaMemcpyToSymbol(c_yParams, &m_yParams, 2*sizeof(double));
  cudaMemcpyToSymbol(c_momentumParams, &m_momentumParams, 6*sizeof(double));
  return;
}

__global__ void parseInData(char* ginput, int* bsize, VeloTracks* vts, FwdHitsEvContainer* fhs) {

  // parse events in parallel                            
  unsigned int event  = blockIdx.x;
  unsigned int idxhit = threadIdx.x;
  unsigned int step   = blockDim.x;
  
  // Velo tracks

  __shared__ int*    noVeloTks;

  __shared__ int*    vt_id;
  __shared__ float*  vt_x0;
  __shared__ float*  vt_tx;
  __shared__ float*  vt_y0;
  __shared__ float*  vt_ty;
  __shared__ float*  vt_z0;
  __shared__ float*  vt_qoverp;

  // Hits
  
  __shared__ int*   noOTHits;
  
  __shared__ int*   oth_LhcbID;
  __shared__ int*   oth_channelID;
  __shared__ float* oth_xAtYEq0;
  __shared__ float* oth_zAtYEq0;
  __shared__ float* oth_yBegin;
  __shared__ float* oth_yEnd;
  __shared__ float* oth_weight;
  __shared__ float* oth_drift;
  __shared__ int*  oth_hasnext;
  __shared__ int*  oth_hasprev;


  // work with thread 0
  if ( idxhit == 0 ) {
    
    noVeloTks       = (int*) (ginput + bsize[event]); 
    vt_id           = (int*) (noVeloTks + 1);
    vt_x0           = (float*) (vt_id + *noVeloTks);
    vt_tx           = (float*) (vt_x0 + *noVeloTks);
    vt_y0           = (float*) (vt_tx + *noVeloTks);
    vt_ty           = (float*) (vt_y0 + *noVeloTks);
    vt_z0           = (float*) (vt_ty + *noVeloTks);
    vt_qoverp       = (float*) (vt_z0 + *noVeloTks);
    
    noOTHits        = (int*) (vt_qoverp + *noVeloTks);
    oth_LhcbID      = (int*) (noOTHits + 1);
    oth_channelID   = (int*) (oth_LhcbID + *noOTHits);
    oth_xAtYEq0     = (float*) (oth_channelID + *noOTHits);
    oth_zAtYEq0     = (float*) (oth_xAtYEq0 + *noOTHits);
    oth_yBegin      = (float*) (oth_zAtYEq0 + *noOTHits);
    oth_yEnd        = (float*) (oth_yBegin + *noOTHits);
    oth_weight      = (float*) (oth_yEnd + *noOTHits);
    oth_drift       = (float*) (oth_weight + *noOTHits);
    oth_hasnext     = (int*) (oth_drift + *noOTHits);
    oth_hasprev     = (int*) (oth_hasnext + *noOTHits);

    vts[event].setSize(*noVeloTks);
    fhs[event].reset();
/*
    printf("We have %d velotk, %d Othits and %d IThits \n", *noVeloTks, *noOTHits, *noITHits);
*/
  }
    
  __syncthreads();

  for (int i = idxhit; i < *noVeloTks; i+=step) 
    vts[event].addTrack(i, vt_id[i], vt_x0[i], vt_y0[i], vt_z0[i], vt_tx[i], vt_ty[i], vt_qoverp[i]);
  
  for (int i = idxhit; i < *noOTHits; i+=step) 
    fhs[event].addHit(i, oth_LhcbID[i], oth_channelID[i], oth_hasnext[i], oth_hasprev[i],  
                      oth_yBegin[i], oth_yEnd[i], oth_xAtYEq0[i], oth_zAtYEq0[i], 
                      oth_drift[i], oth_weight[i]);
                      
  __syncthreads();
  if ( idxhit == 0 ) fhs[event].setnumhits();
  
}

//-------------------------------------------------------------
// initialize all Tracks
__global__ void clearTracks (OutTracks* outputTks) {
  outputTks[blockIdx.x].clear(); 
  return;
}
__global__ void clearTracks (FwdTracks* fwdtracks) {
  fwdtracks[blockIdx.x].clear();
  return;
}

//-------------------------------------------------------------
// FWD kernels

__global__ void find2DCandidates(VeloTracks* velotracks, FwdHitsEvContainer* hits, FwdTracks* fwdtracks ) {

  // go // on velotracks and events
  unsigned int const event = blockIdx.x;
  unsigned int const idxtk = blockIdx.y; // velotrack's index
  
  // sanity checks
  if (idxtk >= velotracks[event].size())
    return;  
    
  __shared__ FwdHits xAtReference;   
  __shared__ unsigned int size;

  
  FwdHitsEvContainer* fwdhits = &hits[event];
  FwdTrack track( velotracks[event].getTrack(idxtk) ); // initialize fwd track with velo seed

 
  if ( threadIdx.x < 36 ) { // use only the first 36 threads
    // loop over 3 stations x 2 layers x 6 regions
    unsigned int const sta = threadIdx.x/12;
    unsigned int const lay = ( ( threadIdx.x % 12 ) / 6 ) * 3;
    unsigned int const region = threadIdx.x % 6;

    // fillXlist

    XInterval interval = make_XInterval(track);
  
    float ty = track.slY();
    float yCompat = c_yCompatibleTol + 50 * fabs(ty);
    float y0 = track.yStraight( 0. );
    float z = fwdhits->zRegion(sta, lay, region);
    float yRegion = track.yStraight( z );
    float minx = interval.xMinAtZ(z);
    float maxx = interval.xMaxAtZ(z);

    //printf("ALE DBG *** search interval: (%f, %f)\n",  interval.xMin(),  interval.xMax());

    if ( fwdhits->isYCompatible(sta, lay, region, yRegion, yCompat) ) {
      float xtol = ( fabs( yRegion * c_sinT ) + 20.);
      for ( int i = 0; i < fwdhits->numHits(sta, lay, region); ++i ) {
        FwdHit hit = fwdhits->getFwdHit(sta, lay, region, i);
        if ( ( hit.x() <  minx - xtol ) || ( hit.x() >  maxx + xtol ) ) continue; 
        //if ( hit.isIgnored() ) continue; // not safe
        updateHitForTrack( fwdhits, &hit, y0, ty );
        if ( !fwdhits->isYCompatible( hit.index(), yRegion, yCompat) ) continue;
        hit.setIgnored( false );
        hit.setRlAmb( 0 );
        if (region > 1 || ( c_minOTDrift < hit.driftDistance() && hit.driftDistance() < c_maxOTDrift ) )
          hit.setSelected( true );
        else {
          hit.setSelected( false );
          continue;
        }
        double xRef = xAtReferencePlane( track, hit, false );
        hit.setProjection( xRef );
        if ( interval.inside( xRef ) ) xAtReference.atomicadd( hit );
        if ( xAtReference.full() ) break; // BTW result can change
      }
    }           
  }
  __syncthreads(); 

  if ( threadIdx.x == 0 ) { 
    // we need at least c_minXPlanes (=5) different planes
    if ( !countPlanes(xAtReference, c_minXPlanes) ) return;
    size = xAtReference.size();
  }
  __syncthreads();
  
  // complete the vector with empty fwdhits (default projection is 99999)
  for ( int s = size+threadIdx.x; s < MaxHitXRef; s+=blockDim.x )
    xAtReference.atomicadd( FwdHit() );
    
  __syncthreads();  
   
  // sort hits by projection 
  for (int k = 2; k <= MaxHitXRef; k *= 2) { //Parallel bitonic sort 
    for (int j = k / 2; j>0; j /= 2) { //Bitonic merge
      for (unsigned int tid=threadIdx.x; tid<MaxHitXRef; tid+=blockDim.x) {
        int ixj = tid ^ j; //XOR
        if (ixj > tid) {
         if ((tid & k) == 0) { // ascending – descending
            if ( xAtReference[tid].projection( ) > xAtReference[ixj].projection( )  ) {
              // swap(shared[tid], shared[ixj]);
              FwdHit tmp = xAtReference[tid];
              xAtReference[tid] = xAtReference[ixj];
              xAtReference[ixj] = tmp;
            }
          } else {
            if ( xAtReference[tid].projection( ) < xAtReference[ixj].projection( ) ) {
                //swap(shared[tid], shared[ixj]);
              FwdHit tmp = xAtReference[tid];
              xAtReference[tid] = xAtReference[ixj];
              xAtReference[ixj] = tmp;
            }
          }
        }  
      }  
      __syncthreads();
    }
  }
  
  __syncthreads(); 
  
  // use only 1 thread x block ==> 1 threads x velo track
  if ( threadIdx.x == 0 ) { 
    int maxXCandidateSize[3] =  {60,30,20};
    //printf("DEBUG ALE ***** We have %d hits..\n", size);
    // buildXCandidatesList 

    CandidateTk cand;

    double xExtrap  = track.xStraight( ZMidT );
    double lastEnd = -1.e10;
    int iP, iE, iB, iF;
  
    for ( iP = 0; iP < size - c_minXPlanes; ++iP) {
  
      iE = iP + c_minXPlanes - 1;
  
      // define the maximal width (last-first) of a cluster
      double spread = c_maxSpreadX + fabs( ( xAtReference[iP].projection() - xExtrap ) * c_maxSpreadSlopeX );
      if ( fwdhits->getFwdHit( xAtReference[iP].index() ).isOT() ) spread += 1.5;  // OT drift ambiguities...

      //== If not enough hits in the maximum spread, skip
      if (  spread < xAtReference[iE].projection() - xAtReference[iP].projection() ) {
        while ( spread < xAtReference[iE].projection() - xAtReference[iP].projection() ) iP++;
        --iP; // as there will be a ++ in the loop !
        continue;
      }

      //== Add all hits inside the maximum spread. If not enough planes, restart
      while ( iE != size && spread > xAtReference[iE].projection() - xAtReference[iP].projection() ) iE++;
      
      FwdCounter planeCount( xAtReference.elements(), iP, iE );
      //== Enough different planes
      if ( c_minXPlanes > planeCount.nbDifferent() ) continue;
    
      //== Try to make a single zone, by removing the first and adding other as
      //   long as the spread and c_minXPlanes conditions are met.
      // => search for overlapping clusters
      iB = iP;
      iF = iE;
      
      while ( iP <= iE - c_minXPlanes && iF < size) {
        planeCount.removeHit( xAtReference[iP] );
        ++iP;
        while ( iF < size && spread > xAtReference[iF].projection() - xAtReference[iP].projection() ) {
          planeCount.addHit( xAtReference[iF] );
          iF++;
        }
        if ( c_minXPlanes <= planeCount.nbDifferent() ) iE = iF;
      }
      
      double x1 = xAtReference[iB].projection();
      double x2 = xAtReference[iE-1].projection();
      
      iP = iE-1;
      
      FwdCounter finalCount( xAtReference.elements(), iB, iE );
      int numplanes = 6-finalCount.nbDifferent();
      int lim;
      if ( 0<= numplanes && numplanes < 3 ) 
        lim = maxXCandidateSize[numplanes];
      else
        continue;  
      for ( int i = iB; i < iE; ++i )
        if ( xAtReference[i].isOT() ) lim--;
        else lim = lim-2;
      
      if ( lim < 0 ) continue;  
      

      //== Protect against too dirty area. -> we eliminate all?
      //if ( iE - iB < c_maxXCandidateSize ) 
      if ( iE - iB <  FWDHITS) {
        //== if the first new point is not close to the last one 
        //== or we have too much hits add a new track candidate
        // else merge hits with the last track candidate
        if ( ( spread < x1 - lastEnd ) ||
             ( cand.back().getNumHits() + iE - iB >= FWDHITS ) )
          cand.add(track);
        
        for( int i = iB; i < iE; ++i )
          cand.back().addHit(xAtReference[i]);     
      
        lastEnd = x2;
      }
    }

    fwdtracks[event].atomicadd( cand );
  }
  return;
}
  
__global__ void  find2Dtracks( FwdHitsEvContainer* hits, FwdTracks* fwdtracks ) {
 
  unsigned int const event = blockIdx.x;
  unsigned int const idxtk = blockIdx.y; // velo track's index
  unsigned int const iL = threadIdx.x;  // T track's index

  // sanity checks
  if ( ( idxtk >= fwdtracks[event].size() ) || 
      ( iL >= fwdtracks[event][idxtk].size() ) ) return;
     
  FwdHitsEvContainer* fwdhits = &hits[event];  
 
  int minOTX = int( 1.5 * c_minXPlanes );

  //== Fit the candidate, and store them
  FwdTrack* tempTk = &fwdtracks[event][idxtk][iL];
  while ( fitXCandidate( fwdhits, tempTk, c_maxChi2, c_minXPlanes ) ) {    
    tempTk->cleanHits();
    double chi2 = chi2PerDoF( fwdhits, tempTk );
    //== Check the chi2 with magnet center constraint.
    if ( c_maxChi2Track > tempTk->chi2PerDoF() ) {
      FwdCounter regions( tempTk->hits(), 0, tempTk->getNumHits());  
      int nbHit = regions.nbOT() + 2*regions.nbIT();
      bool inCenter = c_centerOTYSize > fabs( tempTk->y( 0. ) );
      if ( minOTX <= nbHit || inCenter ) {
        const double momentum = 1.0/fabs(qOverP( *tempTk ));
        const double pt = tempTk->sinTrack()*momentum;
        if ( ( momentum > c_minMomentum && pt > c_minPt ) ) {  
          tempTk->setIgnore( false );
        }
      }
    }
    //== tag these coordinates in the original, so that we don't find the same track again
    tempTk->tagHits();   
  } 

  return;
}

__global__ void find3Dtracks(FwdHitsEvContainer* fwdhits, FwdTracks* fwdtracks, OutTracks* outputTks ) {
 
  unsigned int const event = blockIdx.x; // event's index
  unsigned int const idxtk = blockIdx.y; // velo tk index 
  unsigned int const track = threadIdx.x;// T tk index 

  // sanity checks
  if ( idxtk >= fwdtracks[event].size() || 
    track >= fwdtracks[event][idxtk].size() )
    return;

  FwdHitsEvContainer* hits = &fwdhits[event];

  __shared__ unsigned int size;    
  __shared__ int minPlanes;
  __shared__ int maxPlanes;
  __shared__ int maxOT;
  __shared__ float bestQuality;


  if ( threadIdx.x == 0 ) {
    minPlanes = c_minPlanes;  //== Initial value, can be updated later...  
    maxPlanes = 0;
    bestQuality = 1000.;
    maxOT = 0;
    size = 0;
  }
  __syncthreads();  

   FwdTrack* temp = &fwdtracks[event][idxtk][track];
  if ( temp->ignore() ) return;
 
  temp->setIgnore( true ); // reset all to ignore
  
  for ( int j = 0; j < temp->getNumHits(); ++j ) 
    temp->hitptr(j)->setIgnored( false ); // restore normal flag  
  temp->setSelectedHits( );

  const double qoverp = 1000*qOverP( *temp ); // in 1/GeV
  const double stereotol = c_maxSpreadY + c_maxSpreadSlopeY * qoverp *  qoverp;
    
  // Get the stereo coordinates
  if ( fillStereoList( hits, temp, stereotol ) ) {
    temp->setSelectedHits( );
    //== Fit and reject if not good enough
    if ( fitStereoCandidate( hits, temp, c_maxChi2, minPlanes) ) { 
      temp->cleanHits();
      chi2PerDoF( hits, temp );//== compute and store Chi2PerDoF
      //== Remove stereo coordinates incompatible in Y
      if ( removeYIncompatible( hits, temp, c_yCompatibleTolFinal, minPlanes ) ) {
        temp->cleanHits();
  
        // Enough stereo planes 
        FwdCounter fullCount( temp->hits(), 0, temp->getNumHits());
        int nbY = fullCount.nbStereo();
   
        if ( ( nbY >= 4 ) &&
            ( ( c_maxDeltaY + c_maxDeltaYSlope * qoverp * qoverp ) >= fabs( changeInY( *temp ) ) ) ) {
          temp->setQuality( computeQuality(*temp, qoverp) );

          //== Verify if enough OT measurements, counting IT for 2/plane
          //== Ignore the y central region, OT inefficient there.
          int nbIT = fullCount.nbIT();
          int nbOT = fullCount.nbOT();  
    
          if ( ( inCenter(*temp) ) ||
              ( ( c_minHits <= 2*nbIT+nbOT ) &&
               ( nbIT != 0 || nbOT >= c_minOTHits ) ) ) {

            int nbPlanes = fullCount.nbDifferent();
            atomicMax(&maxPlanes, max( nbPlanes, maxPlanes ));

            //== reject if below threshold
            if ( passMomentum( *temp, temp->sinTrack()) ) {
              //== Update requirement according to already found good solutions... 
              atomicMin(&minPlanes, min( nbPlanes-1, minPlanes ) );
              temp->setIgnore( false ); // good tracks
              atomicInc( &size, MAXTRACKS );
            }
          }  
        } 
      }
    }
  }
  
  //================================================================================
  //  Now some filtering of tracks, in case of multiple candidates.
  //================================================================================
 
  __syncthreads();
  
  if ( !size || temp->ignore() ) return; 

  if ( 1 < size  ) {
    // remove track with sensibly lower number of planes
    FwdCounter tmp( temp->hits(), 0, temp->getNumHits());
    if ( tmp.nbDifferent() >= maxPlanes - 1 ) {
      atomicMin(&bestQuality, min( temp->quality(), bestQuality ));
    } else {
      //printf("[%d, %d] Ignore candidate: not enough planes = %d\n", event, idxtk, tmp.nbDifferent());
      //temp->setIgnore( true ); 
      return; 
    }
            
    __syncthreads();
      
    // remove worst quality
    if ( temp->quality() < bestQuality + 1.0 ) {
      atomicMax(&maxOT, max( 2*tmp.nbIT()+tmp.nbOT(), maxOT ));
    } else {
      //printf("[%d, %d] Ignore candidate: quality too high = %f (> %f)\n", event, idxtk, (*temp).quality(), bestQuality);
      //temp->setIgnore( true );
      return;    
    }  
    
    __syncthreads();      

    // remove if sensibly less OT      
    if ( 2*tmp.nbIT() + tmp.nbOT() <= min( maxOT, 24 ) - 3 )  {
      //  printf("[%d, %d] Ignore candidate: not enough OT = %d (min %d)\n", event, idxtk, tmp.nbOT(), maxOT);
      //temp->setIgnore( true );
      return;
    }
  }
   
  
  //=== Store tracks...

  __syncthreads();    
  
  OutTrack  outTk;
  outTk.vtid = temp->getVeloTk();
  outTk.ax   = temp->ax();
  outTk.bx   = temp->bx();
  outTk.cx   = temp->cx();
  outTk.dx   = temp->dx();
  outTk.ay   = temp->ay();
  outTk.by   = temp->ySlope();
  outTk.chi2xdof = temp->chi2PerDoF();
  outTk.ndof     = temp->nDoF();
  outTk.quality  = temp->quality();
  outTk.valid    = true;
  outTk.HitsNum  = temp->getNumHits();
  for ( int j = 0; j < temp->getNumHits(); ++j )
    outTk.Hits[j] = hits->getLhcbID( temp->hit(j).index() );
  outputTks[event].atomicadd( outTk );
 
  
};

__global__ void removeGhost( OutTracks *input ) {

  // remove worst quality tracks with more than 70% of common T hits

  int const event = blockIdx.y; // event's index
  int const idxtk = blockIdx.x; // track's index
  int const hita = threadIdx.x; // track a hit's index
  int const hitb = threadIdx.y; // track b hit's index

  int const N = input[event].size();

  if ( N < 2 || idxtk > N-1 ) return;

  OutTrack* a = &input[event][idxtk];
  __shared__ unsigned int commonhit;

  int stop = N/2;
  // do not consider the same pairs two times
  if ( ( N%2 == 0 ) && ( idxtk >= N/2 ) )
    stop = N/2 -1;
  
  if ( hita == 0 && hitb == 0) commonhit = 0;
  __syncthreads();

  for (int j = 0; j <= stop; ++j) {
    OutTrack* b = &input[event][ (idxtk+j+1)%N ];
    if ( ( hita < (*a).HitsNum ) &&
         ( hitb < (*b).HitsNum ) &&
         ( (*a).Hits[hita] == (*b).Hits[hitb] ) )
      atomicInc(&commonhit, 32);
    __syncthreads();
    if ( ( hita == 0 ) && ( hitb == 0 ) ) {
      if ( commonhit > 0.7 * min( (*a).HitsNum, (*b).HitsNum) ) {
        if ( (*a).quality > (*b).quality ) {
          (*a).valid = false;
          return;
        }
        else
          (*b).valid = false;
      }
      commonhit = 0;
    }
    __syncthreads(); 
  }
}

























#endif


