PatAlgorithmsCuda: contains the handler and all the cuda kernels, it is run on the server side

PatAlgorithmsGpu: contains the modified files of the "gaudi" algorithm and all the functions to serialize and deserialize data, it is run on the client side

The name of the handler that need to be loaded is: FwdCudaHandler

